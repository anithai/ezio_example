/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.gemalto.ezio.mobile.sdk.example;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.gemalto.ezio.mobile.sdk.example";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 540;
  public static final String VERSION_NAME = "5.4.0";
}
