/*
 * ------------------------------------------------------------------------------
 * <p>
 * Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 * <p>
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 * <p>
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 * <p>
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp;

import static android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_CANCELED;
import static android.hardware.biometrics.BiometricPrompt.BIOMETRIC_ERROR_USER_CANCELED;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeUi;
import com.gemalto.ezio.mobile.sdk.example.otp.task.BioMetricFragment;
import com.gemalto.ezio.mobile.sdk.example.otp.task.GetCapOtpTask;
import com.gemalto.ezio.mobile.sdk.example.otp.task.GetTotpOtpTask;
import com.gemalto.ezio.mobile.sdk.example.otp.task.ProvisioningTask;
import com.gemalto.ezio.mobile.sdk.example.otp.task.SecurePinpadCustomFullScreenFragment;
import com.gemalto.ezio.mobile.sdk.example.otp.task.SecurePinpadCustomFullScreenFragment.onCustomFullScreenListener;
import com.gemalto.ezio.mobile.sdk.example.otp.task.VicVerifyTask;
import com.gemalto.ezio.mobile.sdk.example.util.AsyncResponse;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.authentication.AuthMode;
import com.gemalto.idp.mobile.authentication.AuthenticationModule;
import com.gemalto.idp.mobile.authentication.mode.biofingerprint.BioFingerprintAuthInput;
import com.gemalto.idp.mobile.authentication.mode.biofingerprint.BioFingerprintAuthService;
import com.gemalto.idp.mobile.authentication.mode.biofingerprint.BioFingerprintAuthenticationCallbacks;
import com.gemalto.idp.mobile.authentication.mode.biometric.BiometricAuthInput;
import com.gemalto.idp.mobile.authentication.mode.biometric.BiometricAuthService;
import com.gemalto.idp.mobile.authentication.mode.biometric.BiometricAuthenticationCallbacks;
import com.gemalto.idp.mobile.authentication.mode.biometric.BiometricResultCode;
import com.gemalto.idp.mobile.authentication.mode.pin.PinAuthInput;
import com.gemalto.idp.mobile.authentication.mode.pin.PinAuthService;
import com.gemalto.idp.mobile.authentication.mode.pin.PinRuleException;
import com.gemalto.idp.mobile.authentication.mode.pin.PinRuleIdentical;
import com.gemalto.idp.mobile.authentication.mode.pin.PinRulePalindrome;
import com.gemalto.idp.mobile.authentication.mode.pin.PinRuleSeries;
import com.gemalto.idp.mobile.authentication.mode.pin.PinRuleUniform;
import com.gemalto.idp.mobile.core.ActivationException;
import com.gemalto.idp.mobile.core.ApplicationContextHolder;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.IdpException;
import com.gemalto.idp.mobile.core.root.RootDetector;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.otp.OtpModule;
import com.gemalto.idp.mobile.otp.Token;
import com.gemalto.idp.mobile.otp.oath.OathService;
import com.gemalto.idp.mobile.otp.oath.OathToken;
import com.gemalto.idp.mobile.otp.oath.OathTokenManager;
import com.gemalto.idp.mobile.ui.UiModule;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputBuilderV2;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputService;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputUi;
import com.gemalto.idp.mobile.ui.secureinput.SecurePinpadListenerV2;

public class OtpExampleActivity extends AppCompatActivity
        implements SecurePinpadListenerV2, BioMetricFragment.BioFpFragmentCallback, onCustomFullScreenListener {

    private String TAG = OtpExampleActivity.class.getCanonicalName();

    // Secure Pin pad is used multiple times in this class,
    // Use this SecurePinPadBehavior enum class to determine the reaction of SPP under different context
    private enum PinPadBehavior {
        GENERATE_OTP,
        VERIFY_PIN,
        CHANGE_PIN,
        UPGRADE_TOKEN,
        UNDEFINED
    }

    private OtpSettings mSettings = new OtpSettings();

    //region Member - UI elements
    private EditText regCode;
    private Button provisionButton, getOtpPinButton, changePinButton, activateBioMetricBtn, deactivateBiometricBtn, getOtpBiometricBtn, removeTokenButton;
    private TextView logTextView;

    private View mOtpMainScreen;

    /**
     * Fragment Manager.
     */
    private FragmentManager mFragmentManager;

    private ActionBarDrawerToggle toggle;
    //endregion

    //region Member - OTP objects
    private IdpCore core;
    private Context context;

    private OathService oathService;
    private OathTokenManager oathTokenManager;
    private OathToken oathToken;

//    private CapService capService;
//    private CapTokenManager capTokenManager;
//    private CapToken capToken;

//    private VicService vicService;
//    private VicTokenManager vicTokenManager;
//    private VicToken vicToken;
    //endregion

    //region Member - Biometric objects
    @SuppressWarnings("deprecation")
    private BioFingerprintAuthenticationCallbacks bioFPCallbacks;
    @SuppressWarnings("deprecation")
    private BioFingerprintAuthService bioFPAuthService;
    private BioMetricFragment bioMetricFragment;
    private boolean supportBiometric = true;
    private CancellationSignal cancellationSignal;
    private AuthMode bioAuthMode;
    private BiometricAuthenticationCallbacks bioMPCallbacks;
    private BiometricAuthService bioMPAuthService;
    //endregion

    //region Member - SecurePinPad objects
    private PinPadBehavior pinPadBehavior;
    private SecureInputBuilderV2 pinPadBuilderV2;
    private int pinLength = FakeAppData.getOtpPinLength();
    private String firstLabelText = "Enter PIN";

    private SecureInputUi mSecureInputUi;

    /**
     * The dialogFragment.
     */
    private DialogFragment mDialogFragment;

    /**
     * Frame Layout to hold the Keypad fragment.
     */
    private SecurePinpadCustomFullScreenFragment mCustomKeyPadFragment;

    private SecureString verifyCode;

    private AuthenticationModule authenticationModule;
    private PinAuthService pinAuthService;

    private PinAuthInput currentPin;
    //endregion

    //region Callback - Activity lifecycle
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        loadView();

        context = ApplicationContextHolder.getContext();

        loadConfiguration();
        checkDeviceCompatibility(); // Check if device supports Bio FP
        loadExistingTokens();
        loadListener();

        updateMenuState();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Tools.hideKeyboard(OtpExampleActivity.this, regCode);

        toggle.onOptionsItemSelected(item);

        return false;
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Security Guideline: AND01. Sensitive data leaks
        logTextView.setText("\tCleared for security reasons\n");

        // Wipe password storage
        if (currentPin != null) {

            //GEN08.PIN sanitisation
            currentPin.wipe();

            // Set back pin pad behavior in case application go to background after PIN conformation
            // User must enter old PIN again to change the PIN if the application come to foreground
            pinPadBehavior = PinPadBehavior.UNDEFINED;
        }

        // Security Guideline: AND08. Secure Pin Pad
        if (pinPadBuilderV2 != null)
            pinPadBuilderV2.wipe();
        if (mSettings.getDisplayMode() != null) {
            switch (mSettings.getDisplayMode()) {
                case Dialog: {
                    dismissPinPadDialog();
                    break;
                }

                case Fullscreen: {
                    // dismiss keypad, user can write your own dismiss method. User has to
                    // dismiss it since the dialogFragment cannot be reused.
                    // To user it, you have to create a new dialogFragment from pinpad
                    // object.
                    dismissKeyPad();
                    break;
                }

                case CustomFullscreen: {
                    dismissCustomKeyPad();
                    break;
                }

                default:
                    break;
            }
        }

        if (supportBiometric) {

            if (bioMetricFragment != null
                    && bioMetricFragment.getDialog() != null
                    && bioMetricFragment.getDialog().isShowing()) {
                bioMetricFragment.getDialog().dismiss();
            }
            cancelBioFp();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Wipe away sensitive information
        if (currentPin != null) {
            currentPin.wipe();
        }
    }

    @Override
    public void onBackPressed() {
        do {
            /*
             * NOTE: For SecurePinPad dialog mode, as it is important to input the data so 3rd application can not handle the back key.
             * In other words, in the dialog mode, the user has to input something to dismiss the dialog!
             */
            boolean processed = false;
            switch (mSettings.getDisplayMode()) {
                case Fullscreen: {
                    processed = dismissKeyPad();
                    break;
                }

                case CustomFullscreen: {
                    processed = dismissCustomKeyPad();
                    break;
                }

                default:
                    break;
            }
            if (processed)
                break;

            super.onBackPressed();
        } while (false);
    }

    //endregion

    //region Private - UI utilities
    private void loadView() {
        setContentView(R.layout.activity_otp_example);

        regCode = findViewById(R.id.reg_code);
        provisionButton = findViewById(R.id.provision);
        getOtpPinButton = findViewById(R.id.getOtpByPin);
        changePinButton = findViewById(R.id.changePin);
        activateBioMetricBtn = findViewById(R.id.activateBiometric);
        deactivateBiometricBtn = findViewById(R.id.deactivateBiometric);
        getOtpBiometricBtn = findViewById(R.id.getOtpByBiometric);
        removeTokenButton = findViewById(R.id.removeToken);
        logTextView = findViewById(R.id.log);

        mFragmentManager = getSupportFragmentManager();
        mOtpMainScreen = findViewById(R.id.otp_main_layout);

        //region Initialise DrawerLayout
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        DrawerLayout drawer = findViewById(R.id.otp_drawer);
        toggle = new ActionBarDrawerToggle(this, drawer, R.string.app_name, R.string.app_name);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //endregion

        //region Initialise menu callback
        View.OnClickListener mnuCallback = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();

                ToggleButton tbSimulateOk = findViewById(R.id.spp_opt_simulate_ok);
                ToggleButton tbTouchRipple = findViewById(R.id.spp_opt_touch_ripple);

                switch (id) {
                    case R.id.otp_totp:
                        mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.TOTP);
                        break;

                    case R.id.otp_cap:
                        mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.CAP);
                        break;

                    case R.id.otp_vic:
                        mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.VIC);
                        break;

                    case R.id.spp_mode_custom_fullscreen:
                        mSettings.setDisplayMode(OtpSettings.ENU_DISPLAY_MODE.CustomFullscreen);

                        tbSimulateOk.setEnabled(false);
                        tbSimulateOk.setChecked(false);
                        tbTouchRipple.setEnabled(false);
                        tbTouchRipple.setChecked(false);

                        break;

                    case R.id.spp_mode_dialog:
                        mSettings.setDisplayMode(OtpSettings.ENU_DISPLAY_MODE.Dialog);

                        tbSimulateOk.setEnabled(true);
                        tbSimulateOk.setChecked(mSettings.isSimulateOkButtonPress());
                        tbTouchRipple.setEnabled(true);
                        tbTouchRipple.setChecked(mSettings.isTouchRippleSetting());
                        break;

                    case R.id.spp_mode_fullscreen:
                        mSettings.setDisplayMode(OtpSettings.ENU_DISPLAY_MODE.Fullscreen);

                        tbSimulateOk.setEnabled(true);
                        tbSimulateOk.setChecked(mSettings.isSimulateOkButtonPress());
                        tbTouchRipple.setEnabled(true);
                        tbTouchRipple.setChecked(mSettings.isTouchRippleSetting());
                        break;

                    case R.id.spp_opt_scramble:
                        mSettings.setScrambleKeys(((ToggleButton) findViewById(id)).isChecked());
                        break;

                    case R.id.spp_opt_simulate_ok:
                        mSettings.setSimulateOkButtonPress(((ToggleButton) findViewById(id)).isChecked());
                        break;

                    case R.id.spp_opt_swap_control:
                        mSettings.setSwapControlButtons(((ToggleButton) findViewById(id)).isChecked());
                        break;

                    case R.id.spp_opt_touch_ripple:
                        mSettings.setTouchRippleSetting(((ToggleButton) findViewById(id)).isChecked());
                        break;

                    case R.id.spp_opt_visualise_press:
                        mSettings.setVisualizeButtonPress(((ToggleButton) findViewById(id)).isChecked());
                        break;

                    case R.id.spp_opt_enable: {
                        boolean enabledSPP = ((ToggleButton) findViewById(id)).isChecked();
                        mSettings.setEnableSecurePinPad(enabledSPP);
                        enableSecurePinPadOption(enabledSPP, findViewById(R.id.spp_opt_container));
                        break;
                    }

                    default:
                        break;
                }
            }
        };

        int[] ids = new int[]{
                R.id.otp_totp,
                R.id.otp_cap,
                R.id.otp_vic,

                R.id.spp_opt_enable,
                R.id.spp_mode_custom_fullscreen,
                R.id.spp_mode_dialog,
                R.id.spp_mode_fullscreen,
                R.id.spp_opt_scramble,
                R.id.spp_opt_simulate_ok,
                R.id.spp_opt_swap_control,
                R.id.spp_opt_touch_ripple,
                R.id.spp_opt_visualise_press,
        };
        for (int id : ids)
            findViewById(id).setOnClickListener(mnuCallback);
        //endregion
    }

    private void enableSecurePinPadOption(boolean enabled, View parent) {
        do {
            if (!(parent instanceof ViewGroup)) {
                parent.setEnabled(enabled);
                break;
            }

            for (int i = 0; i < ((ViewGroup) parent).getChildCount(); i++)
                enableSecurePinPadOption(enabled, ((ViewGroup) parent).getChildAt(i));
        } while (false);
    }

    private void loadListener() {
        provisionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doProvisioning();
            }
        });

        getOtpPinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateOtpByPin();
            }
        });

        changePinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePin();
            }
        });

        activateBioMetricBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeBiometric();
            }
        });

        deactivateBiometricBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deactivateBiometric();
            }
        });

        getOtpBiometricBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateOtpByBiometric();
            }
        });

        // Register remove token listener to the remove token button
        removeTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.hideKeyboard(context, regCode);

                removeExistingTokens();
            }
        });
    }

    /**
     * Update menu UI state based on the OTP Settings
     */
    private void updateMenuState() {
        //region Menu settings of OTP type
        RadioButton rbTOTP, rbCAP, rbVIC;

        rbTOTP = findViewById(R.id.otp_totp);
        rbTOTP.setChecked(mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.TOTP);

        rbCAP = findViewById(R.id.otp_cap);
        rbCAP.setChecked(mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.CAP);

        rbVIC = findViewById(R.id.otp_vic);
        rbVIC.setChecked(mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.VIC);

        // Token is existing, disable this option
        boolean clickable = (oathToken == null /*&&
                capToken == null &&
                vicToken == null*/);
        rbTOTP.setEnabled(clickable);
//        rbCAP.setEnabled(clickable);
//        rbVIC.setEnabled(clickable);
        //endregion

        //region Menu settings for SecurePinPad
        ((ToggleButton) findViewById(R.id.spp_opt_enable)).setChecked(mSettings.isEnableSecurePinPad());
        ((ToggleButton) findViewById(R.id.spp_opt_scramble)).setChecked(mSettings.isScrambleKeys());
        ((ToggleButton) findViewById(R.id.spp_opt_swap_control)).setChecked(mSettings.isSwapControlButtons());
        ((ToggleButton) findViewById(R.id.spp_opt_visualise_press)).setChecked(mSettings.isVisualizeButtonPress());
        ToggleButton tbSimulateOk, tbTouchRipple;
        tbSimulateOk = findViewById(R.id.spp_opt_simulate_ok);
        tbTouchRipple = findViewById(R.id.spp_opt_touch_ripple);
        tbSimulateOk.setChecked(mSettings.isSimulateOkButtonPress());
        tbTouchRipple.setChecked(mSettings.isTouchRippleSetting());

        ((RadioButton) findViewById(R.id.spp_mode_dialog)).setChecked(mSettings.getDisplayMode() == OtpSettings.ENU_DISPLAY_MODE.Dialog);
        ((RadioButton) findViewById(R.id.spp_mode_fullscreen)).setChecked(mSettings.getDisplayMode() == OtpSettings.ENU_DISPLAY_MODE.Fullscreen);
        ((RadioButton) findViewById(R.id.spp_mode_custom_fullscreen)).setChecked(mSettings.getDisplayMode() == OtpSettings.ENU_DISPLAY_MODE.CustomFullscreen);

        enableSecurePinPadOption(mSettings.isEnableSecurePinPad(), findViewById(R.id.spp_opt_container));

        // if custom full screen mode be selected, disable simulate ok button and touch ripple button
        if (mSettings.getDisplayMode() == OtpSettings.ENU_DISPLAY_MODE.CustomFullscreen) {
            tbSimulateOk.setEnabled(false);
            tbSimulateOk.setChecked(false);

            tbTouchRipple.setEnabled(false);
            tbTouchRipple.setChecked(false);
        }
        //endregion
    }

    //region PIN without using SecurePinPad

    /**
     * Displaying the pin input ***NOT*** using SecurePinPad
     */
    @SuppressWarnings("InflateParams")
    private void showRawPinInput() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogLayout = inflater.inflate(R.layout.verifycode_input_dialog, null);

        final EditText pinCodeTxt = dialogLayout.findViewById(R.id.txt_code);
        pinCodeTxt.setHint(R.string.txt_raw_pin_enter_pin_hint_label);
        pinCodeTxt.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(FakeAppData.getOtpPinLength()),
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogLayout);
        builder.setTitle(R.string.please_provide_your_pin);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Tools.hideKeyboard(context, pinCodeTxt);
                        dialog.dismiss();

                        String rawPinString = pinCodeTxt.getText().toString();
                        // Construct the PIN object
                        PinAuthInput pin = pinAuthService.createAuthInput(rawPinString);

                        processSecurePinPadResult(pin, null);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Tools.hideKeyboard(context, pinCodeTxt);
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Tools.showKeyboard(context, pinCodeTxt);
            }
        });
        dialog.show();
    }

    /**
     * Displaying the change pin ***NOT*** using SecurePinPad
     */
    @SuppressWarnings("InflateParams")
    private void showRawPinChangePin() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogLayout = inflater.inflate(R.layout.verifycode_input_dialog, null);

        final EditText pinCodeTxt = dialogLayout.findViewById(R.id.txt_code);
        pinCodeTxt.setHint(R.string.txt_raw_pin_enter_new_pin_hint_label);
        pinCodeTxt.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(FakeAppData.getOtpPinLength()),
        });

        final EditText confirmPinCodeTxt = dialogLayout.findViewById(R.id.txt_confirm_code);
        confirmPinCodeTxt.setVisibility(View.VISIBLE);
        confirmPinCodeTxt.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(FakeAppData.getOtpPinLength()),
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogLayout);
        builder.setTitle(R.string.please_provide_your_pin);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        String rawPinString;

                        // Construct the PIN1 object
                        rawPinString = pinCodeTxt.getText().toString();
                        PinAuthInput pin1 = pinAuthService.createAuthInput(rawPinString);

                        // Construct the PIN2 object
                        rawPinString = confirmPinCodeTxt.getText().toString();
                        PinAuthInput pin2 = pinAuthService.createAuthInput(rawPinString);

                        // Process to next step
                        Tools.hideKeyboard(context, pinCodeTxt);
                        processSecurePinPadResult(pin1, pin2);
                    }
                });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Tools.showKeyboard(context, pinCodeTxt);
            }
        });
        dialog.show();
    }
    //endregion

    //region Pin fall back
    @SuppressLint("InflateParams")
    private void showPinFallbackDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View promptsView = inflater.inflate(R.layout.pin_fallback_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        generateOtpByPin();
                    }
                });
        alertDialogBuilder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }
    //endregion

    //endregion

    //region EZIO configuration and Token reload

    /**
     * Initialise the EZIO core for using OTP and SecurePinPad feature
     */
    @SuppressWarnings("ObsoleteSdkInt")
    private void loadConfiguration() {
        // Security Guideline: AND01. Sensitive data leaks
        // Prevents screenshots of the app
        if (Build.VERSION.SDK_INT >= 11) {
            Tools.disableScreenShot(this);
        }

        //region securePinPad configuration
        // Initialise the behavior for secure pin pad
        pinPadBehavior = PinPadBehavior.UNDEFINED;

        // Create AuthenticationModule. It's the entry point for all authentication relate features.
        authenticationModule = AuthenticationModule.create();
        pinAuthService = PinAuthService.create(authenticationModule);
        //endregion

        //region OTP module configuration
        // Create OtpModule. It's the entry point for all Otp relate features.
        OtpModule otpModule = OtpModule.create();

        //region TOTP module configuration
        // Create OathService. It's the entry point for all oath relate features.
        oathService = OathService.create(otpModule);
        // Create OathTokenManager.
        oathTokenManager = oathService.getTokenManager();
        //endregion

        //region CAP module configuration
        // Create CapService. It's the entry point for all Cap relate features.
//        capService = CapService.create(otpModule);
        // Create CapTokenManager.
//        capTokenManager = capService.getTokenManager();
        //endregion

        //region VIV module configuration
        // Create VicService. It's the entry point for all Vic relate features.
//        vicService = VicService.create(otpModule);
        // Create VicTokenManager.
//        vicTokenManager = vicService.getTokenManager();
        //endregion
        //endregion

        //region core configuration
        core = IdpCore.getInstance();

        // Sensitive information can be wiped here if device is rooted
        if (core.getRootDetector().getRootStatus() == RootDetector.RootStatus.ROOTED) {
            removeExistingTokens();

            // Disable all the buttons
            provisionButton.setEnabled(false);
            getOtpPinButton.setEnabled(false);
            changePinButton.setEnabled(false);
            activateBioMetricBtn.setEnabled(false);
            deactivateBiometricBtn.setEnabled(false);
            getOtpBiometricBtn.setEnabled(false);

            // Besides the root policy configured, application can decide what to do when device is rooted
            logTextView.append("Device detected as rooted\n");
        } else {
            logTextView.append("Device detected as clean\n");
        }
        //endregion
    }

    /**
     * Initialise previous (tokens) data
     */
    private void loadExistingTokens() {
        //region Retrieve the token if any; only take the first token
        do {
            String tokenName;
            oathToken = null;
//            vicToken = null;
//            capToken = null;

            try {
                if (oathTokenManager.getTokenNames().size() > 0) {
                    tokenName = FakeUi.selectTokenName(oathService);
                    oathToken = oathTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());

                    mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.TOTP);
                    break;
                }
//
//                if (capTokenManager.getTokenNames().size() > 0) {
//                    tokenName = FakeUi.selectTokenName(capService);
//                    capToken = capTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());
//
//                    mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.CAP);
//                    break;
//                }
//
//                if (vicTokenManager.getTokenNames().size() > 0) {
//                    tokenName = FakeUi.selectTokenName(vicService);
//                    vicToken = vicTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());
//
//                    mSettings.setOtpType(OtpSettings.ENU_OTP_TYPE.VIC);
//                    break;
//                }
            } catch (IdpException ex) {
                logTextView.append("\tUnable to load the token from storage with exception: " + ex.getClass().getSimpleName() +
                        "\n\tMessage: " + ex.getMessage() + "\n");
            }
        } while (false);
        //endregion

        //region Update UI based on the token state

        // Check whether there are existing tokens,
        // If there are any, disable provisioning and using existing token
        // Otherwise, enable provisioning button

        if ((oathToken == null) /*&& (capToken == null) && (vicToken == null)*/) {
            logTextView.append("\tToken is null. Please provision again\n");

            provisionButton.setEnabled(true);
            regCode.setEnabled(true);

            getOtpPinButton.setEnabled(false);
            changePinButton.setEnabled(false);
            activateBioMetricBtn.setEnabled(false);
            deactivateBiometricBtn.setEnabled(false);
            getOtpBiometricBtn.setEnabled(false);
            removeTokenButton.setEnabled(false);
        } else {
            logTextView.append("\tToken is loaded from database\n");

            provisionButton.setEnabled(false);
            Tools.hideKeyboard(context, regCode);
            regCode.setEnabled(false);
            regCode.setHint(R.string.txt_registration_code_entered_hint);

            getOtpPinButton.setEnabled(true);
            changePinButton.setEnabled(true);
            removeTokenButton.setEnabled(true);

            try {
                if ((mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.TOTP && oathToken.isMultiAuthModeEnabled() && oathToken.isAuthModeActive(bioAuthMode))/*
                        || (mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.CAP && capToken.isMultiAuthModeEnabled() && capToken.isAuthModeActive(bioAuthMode))
                        || (mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.VIC && vicToken.isMultiAuthModeEnabled() && vicToken.isAuthModeActive(bioAuthMode))*/) {
                    activateBioMetricBtn.setEnabled(false);
                    deactivateBiometricBtn.setEnabled(true);
                    getOtpBiometricBtn.setEnabled(true);
                } else {
                    activateBioMetricBtn.setEnabled(true);
                    deactivateBiometricBtn.setEnabled(false);
                    getOtpBiometricBtn.setEnabled(false);
                }
            } catch (IdpException ex) {
                logTextView.append("\tUnable to retrieve the Biometric state of the token with exception: " + ex.getClass().getSimpleName() +
                        "\n\tMessage: " + ex.getMessage() + "\n");
            }

        }

        updateMenuState();

        //endregion
    }
    //endregion

    //region Listener implementation

    /**
     * Step 1: Do provisioning to get token first
     */
    private void doProvisioning() {

        Tools.hideKeyboard(context, regCode);

        do {
            String txtRegistrationCode = regCode.getText().toString();

            // Check if the registration code is valid or not
            if (TextUtils.isEmpty(txtRegistrationCode)) {
                logTextView.append("\nThe Registration Code cannot be empty!\n");
                break;
            }

            // This example places all SDK examples in a background task. Typically,
            // only operations noted as requiring significantly execution time (e.g.
            // TokenManager.createToken) need to execute in a background task.
            SecureString registrationCode = core.getSecureContainerFactory().fromString(txtRegistrationCode);
            ProvisioningTask provisionTask = new ProvisioningTask(mSettings.getOtpType(), registrationCode, logTextView,
                    new AsyncResponse() {

                        @Override
                        // This function receives the token name passed by Provisioning task
                        // Also it will enable the get OTP and change pin button if provision is success
                        public void processFinish(String tokenName) {
                            // Get token name passed by provisioning task class
                            if (TextUtils.isEmpty(tokenName)) {
                                logTextView.append("\tProvisioning failed.\n");
                                return;
                            }

                            try {
                                switch (mSettings.getOtpType()) {
//                                    case CAP:
//                                        capToken = capTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());
//                                        break;

                                    case TOTP:
                                        oathToken = oathTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());
                                        break;

//                                    case VIC:
//                                        vicToken = vicTokenManager.getToken(tokenName, FakeAppData.getCustomFingerprintData());
//                                        break;

                                    default:
                                        break;
                                }
                            } catch (IdpException e) {
                                logTextView.append("\tFailed to get the token, exception: " + e.getClass().getSimpleName() +
                                        "\n\tMessage: " + e.getMessage() + "\n");
                            }

                            //Provisioning task thread don't have access to UI views in main activity
                            //The following statements make sure the UI element can be modified
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Update UI: Clear registration code in edit text since we are not using it anymore
                                        regCode.setText("");
                                        regCode.setHint(getString(R.string.txt_registration_code_entered_hint));
                                        regCode.setEnabled(false);

                                        // Enable get OTP button and change pin button
                                        provisionButton.setEnabled(false);
                                        getOtpPinButton.setEnabled(true);
                                        changePinButton.setEnabled(true);
                                        removeTokenButton.setEnabled(true);
                                        if (supportBiometric) {
                                            activateBioMetricBtn.setEnabled(true);
                                            deactivateBiometricBtn.setEnabled(false);
                                            getOtpBiometricBtn.setEnabled(false);
                                        }

                                        updateMenuState();
                                    }
                                });
                            } catch (Exception e) {
                                logTextView.append("\t" + e.getMessage() + "\n");
                            }
                        }
                    });
            provisionTask.execute();
        } while (false);
    }

    /**
     * Step 2 Get OTP by PIN.
     */
    private void generateOtpByPin() {
        do {
            Tools.hideKeyboard(context, regCode);

            logTextView.append(Tools.generateLogTitle("Generate OTP by Pin"));

            try {
                // Define secure pin pad behavior
                pinPadBehavior = PinPadBehavior.GENERATE_OTP;
                switch (mSettings.getOtpType()) {
                    case TOTP:
                    case CAP: {
                        if (!mSettings.isEnableSecurePinPad()) {
                            showRawPinInput();
                        } else {
                            showKeypadBuilder();
                        }
                        break;
                    }

                    case VIC: {
                        showVerifyCodeDialog();
                        break;
                    }

                    default:
                        break;
                }
            } catch (ActivationException e) {
                logTextView.append("\tSecure Pin Pad is not enabled.\n");
            }
        } while (false);
    }

    /**
     * Step 3 Change PIN.
     */
    private void changePin() {
        do {
            Tools.hideKeyboard(context, regCode);

            // Display secure pin pad to verify old pin
            try {
                logTextView.append(Tools.generateLogTitle("Change PIN"));

                //First step is to verify the old PIN
                pinPadBehavior = PinPadBehavior.VERIFY_PIN;
                if (!mSettings.isEnableSecurePinPad()) {
                    showRawPinInput();
                } else {
                    showKeypadBuilder();
                }
                Log.e(TAG, "changePin: " + mSettings.isEnableSecurePinPad());
            } catch (ActivationException e) {
                logTextView.append("\tSecure Pin Pad is not enabled.\n");
            }
        } while (false);
    }

    /**
     * Step 4
     * Request pin to upgrade to token to support Multi-Authentication mode
     * In a real application, it is recommended to verify the PIN with an OTP first
     * before the actual migration. After which bioFingerprint mode is activated
     */
    private void activeBiometric() {
        Tools.hideKeyboard(context, regCode);

        // Display secure pin pad to verify old pin
        logTextView.append(Tools.generateLogTitle("Activate Biometric"));
        try {
            // Define secure pin pad behavior
            pinPadBehavior = PinPadBehavior.UPGRADE_TOKEN;
            if (!mSettings.isEnableSecurePinPad()) {
                showRawPinInput();
            } else {
                showKeypadBuilder();
            }
        } catch (ActivationException e) {
            logTextView.append("\tSecure Pin Pad is not enabled.\n");
        }
    }

    /**
     * Step 5 Get OTP by Biometric.
     */
    @TargetApi(23)
    private synchronized void generateOtpByBiometric() {
        try {
            Tools.hideKeyboard(context, regCode);

            // Avoid starting the dialog twice
            if (bioMetricFragment != null
                    && bioMetricFragment.getDialog() != null
                    && bioMetricFragment.getDialog().isShowing()) {
                return;
            }
            logTextView.append(Tools.generateLogTitle("Generate OTP by Biometric"));

            // Setup to receive cancellation signal from FP Sensor
            cancellationSignal = new CancellationSignal();

            if ((mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.TOTP) && (oathToken.isMultiAuthModeEnabled())) {
                bioAuthenticateUser(oathToken);
            } /*else if ((mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.CAP) && (capToken.isMultiAuthModeEnabled())) {
                bioAuthenticateUser(capToken);
            } else if ((mSettings.getOtpType() == OtpSettings.ENU_OTP_TYPE.VIC) && (vicToken.isMultiAuthModeEnabled())) {
                bioAuthenticateUser(vicToken);
            }*/ else {
                logTextView.append("Token is not activated for Bio Fingerprint mode.");
            }
        } catch (IdpException e) {
            logTextView.append("\tFailed to generate OTP with Biometric, exception: " + e.getClass().getSimpleName() +
                    "\n\tMessage: " + e.getMessage() + "\n");
        }
    }

    /**
     * Step 6 (Optional)
     * Deactivate Fingerprint authentication mode of the token.
     */
    private void deactivateBiometric() {
        logTextView.append(Tools.generateLogTitle("Deactivate Biometric"));
        try {
            boolean authModeDeactivated = false;
            switch (mSettings.getOtpType()) {
                case TOTP:
                    if (oathToken.isMultiAuthModeEnabled() && oathToken.isAuthModeActive(bioAuthMode)) {
                        oathToken.deactivateAuthMode(bioAuthMode);
                        authModeDeactivated = true;
                    }
                    break;
//                case CAP:
//                    if (capToken.isMultiAuthModeEnabled() && capToken.isAuthModeActive(bioAuthMode)) {
//                        capToken.deactivateAuthMode(bioAuthMode);
//                        authModeDeactivated = true;
//                    }
//                    break;
//                case VIC:
//                    if (vicToken.isMultiAuthModeEnabled() && vicToken.isAuthModeActive(bioAuthMode)) {
//                        vicToken.deactivateAuthMode(bioAuthMode);
//                        authModeDeactivated = true;
//                    }
//                    break;
                default:
                    break;
            }
            if (authModeDeactivated) {
                logTextView.append("\tBio Fingerprint mode deactivated successfully.\n");
                activateBioMetricBtn.setEnabled(true);
                getOtpBiometricBtn.setEnabled(false);
                deactivateBiometricBtn.setEnabled(false);
            }
        } catch (IdpException e) {
            logTextView.append("\tFailed to deactivate Biometric, exception: " + e.getClass().getSimpleName() +
                    "\n\tMessage: " + e.getMessage() + "\n");
        }
    }

    /**
     * Step 7 Remove Token.
     */
    private void removeExistingTokens() {
        // Oath token
        try {
            logTextView.append(Tools.generateLogTitle("Remove Token"));

            //region Remove all tokens
            OathTokenManager oathMgr = oathService.getTokenManager();
            for (String name : oathMgr.getTokenNames()) {
                oathMgr.removeToken(name);
            }
            oathToken = null;

//            CapTokenManager capMgr = capService.getTokenManager();
//            for (String name : capMgr.getTokenNames()) {
//                capMgr.removeToken(name);
//            }
//            capToken = null;
//
//            VicTokenManager vicMgr = vicService.getTokenManager();
//            for (String name : vicMgr.getTokenNames()) {
//                vicMgr.removeToken(name);
//            }
//            vicToken = null;
            //endregion

            logTextView.append("\tToken has been removed.\n");

            provisionButton.setEnabled(true);
            regCode.setEnabled(true);

            getOtpPinButton.setEnabled(false);
            changePinButton.setEnabled(false);
            activateBioMetricBtn.setEnabled(false);
            deactivateBiometricBtn.setEnabled(false);
            getOtpBiometricBtn.setEnabled(false);
            removeTokenButton.setEnabled(false);

            updateMenuState();
        } catch (IdpException e) {
            logTextView.append("\tFailed to remove token, exception: " + e.getClass().getSimpleName() +
                    "\n\tMessage: " + e.getMessage() + "\n");
        }
    }
    //endregion

    //region BioMetric Callback

    /**
     * BioFPFragmentDialog Callback Method
     * Handle dialog cancellation here
     */
    @Override
    public void onCancel() {
        // Handle the Bio Fingerprint cancel dialog here
        cancelBioFp();
    }
    //endregion

    //region BioMetric Logic
    @TargetApi(23)
    private void cancelBioFp() {
        if (cancellationSignal != null && !cancellationSignal.isCanceled()) {
            cancellationSignal.cancel();
        }

        cancellationSignal = null;
    }

    /**
     * Get authentication mode.
     *
     * @return The AuthMode
     */
    public AuthMode getAuthMode() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            return bioFPAuthService.getAuthMode();
        } else {
            return bioMPAuthService.getAuthMode();
        }
    }

    @SuppressWarnings("deprecation")
    private void checkDeviceCompatibility() {
        bioFPAuthService = BioFingerprintAuthService.create(authenticationModule);
        bioMetricFragment = BioMetricFragment.newInstance(this, "Generate OTP");

        bioMPAuthService = BiometricAuthService.create(authenticationModule);
        bioAuthMode = getAuthMode();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            if (bioFPAuthService.isSupported() && bioFPAuthService.isConfigured()) {
                supportBiometric = true;
                // Instantiate Bio FP Callbacks
                bioFPCallbacks = setupBioFPCallback();
            } else {
                supportBiometric = false;
            }
        } else {
            if (BiometricResultCode.BIOMETRIC_SUCCESS == bioMPAuthService.canAuthenticate()) {
                supportBiometric = true;
                // Instantiate Bio FP Callbacks
                bioMPCallbacks = setupBioMPCallback();
            } else {
                supportBiometric = false;
            }
        }
    }

    /**
     * Callback method that must be implemented in order to use Bio Fingerprint Authentication
     */
    @SuppressWarnings("deprecation")
    private BioFingerprintAuthenticationCallbacks setupBioFPCallback() {
        return new BioFingerprintAuthenticationCallbacks() {

            // Fingerprint authenticated successfully.
            // Proceed to get OTP
            @Override
            public void onSuccess(BioFingerprintAuthInput bioFingerprintAuthInput) {
                if (bioMetricFragment != null) {
                    bioMetricFragment.dismiss();
                }

                switch (mSettings.getOtpType()) {
                    case TOTP:
                        GetTotpOtpTask getTotpOtpTask = new GetTotpOtpTask(bioFingerprintAuthInput, logTextView);
                        getTotpOtpTask.execute();
                        break;
                    case CAP:
                        GetCapOtpTask getCapOtpTask = new GetCapOtpTask(bioFingerprintAuthInput, logTextView);
                        getCapOtpTask.execute();
                        break;
                    case VIC:
                        VicVerifyTask vicVerifyTask = new VicVerifyTask(verifyCode, bioFingerprintAuthInput, logTextView);
                        vicVerifyTask.execute();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onStartFPSensor() {
                bioMetricFragment.show(getSupportFragmentManager(), "bioMetricFragment");
            }

            @Override
            public void onError(IdpException e) {
                logTextView.append("\tFingerprint error, exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
            }

            @Override
            public void onAuthenticationError(int i, CharSequence charSequence) {

                /*
                Refer to the official Android Fingerprint API for more error codes.
                https://developer.android.com/reference/android/hardware/fingerprint/FingerprintManager.html
                */
                if (i == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                    bioMetricFragment.setPromptText("Too many failed attempts. Use Pin instead.");
                } else if ((i == FingerprintManager.FINGERPRINT_ERROR_USER_CANCELED) || (i == FingerprintManager.FINGERPRINT_ERROR_CANCELED)) {
                    if (bioMetricFragment != null
                            && bioMetricFragment.getDialog() != null
                            && bioMetricFragment.getDialog().isShowing()) {
                        bioMetricFragment.getDialog().dismiss();
                    }
                    showPinFallbackDialog();
                }
            }

            @Override
            public void onAuthenticationHelp(int i, CharSequence charSequence) {
                bioMetricFragment.setPromptText(charSequence.toString());
            }

            @Override
            public void onAuthenticationSucceeded() {
                bioMetricFragment.setPromptText("Fingerprint recognized.");
            }

            @Override
            public void onAuthenticationFailed() {
                bioMetricFragment.setPromptText("Fingerprint not recognized. Try again.");
            }
        };
    }

    /**
     * Callback method that must be implemented in order to use Biometric Authentication
     */
    private BiometricAuthenticationCallbacks setupBioMPCallback() {
        return new BiometricAuthenticationCallbacks() {

            // Fingerprint authenticated successfully.
            // Proceed to get OTP
            @Override
            public void onSuccess(BiometricAuthInput biometricAuthInput) {
                switch (mSettings.getOtpType()) {
                    case TOTP:
                        GetTotpOtpTask getTotpOtpTask = new GetTotpOtpTask(biometricAuthInput, logTextView);
                        getTotpOtpTask.execute();
                        break;
                    case CAP:
                        GetCapOtpTask getCapOtpTask = new GetCapOtpTask(biometricAuthInput, logTextView);
                        getCapOtpTask.execute();
                        break;
                    case VIC:
                        VicVerifyTask vicVerifyTask = new VicVerifyTask(verifyCode, biometricAuthInput, logTextView);
                        vicVerifyTask.execute();
                        break;
                    default:
                        break;
                }
                logTextView.append("\tbioAuthenticate onSuccess\n");
            }

            @Override
            public void onError(IdpException e) {
                logTextView.append("\tbioAuthenticate onError, exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
            }

            @Override
            public void onAuthenticationError(int var1, CharSequence var2) {

                /*
                Refer to the official Android Biometric API for more error codes.
                https://developer.android.com/reference/android/hardware/biometrics/BiometricPrompt.html
                */
                if ((var1 == BIOMETRIC_ERROR_USER_CANCELED) || (var1 == BIOMETRIC_ERROR_CANCELED)) {
                    showPinFallbackDialog();
                } else {
                    cancellationSignal.cancel();
                    logTextView.append("\tbioAuthenticate onAuthenticationError: " + var1 + " - " + var2 + "\n");
                }
            }

            @Override
            public void onAuthenticationHelp(int var1, CharSequence var2) {
                logTextView.append("\tbioAuthenticate onAuthenticationHelp: " + var1 + " - " + var2 + "\n");
            }

            @Override
            public void onAuthenticationSucceeded() {
            }

            @Override
            public void onAuthenticationFailed() {
                logTextView.append("\tbioAuthenticate onAuthenticationFailed\n");
            }
        };
    }

    private void bioAuthenticateUser(Token token) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            bioFPAuthService.getBioFingerprintContainer().authenticateUser(
                    token,
                    cancellationSignal,
                    bioFPCallbacks);
        } else {
            bioMPAuthService.getBiometricContainer().authenticateUser(
                    token,
                    "Test Biometric",
                    "Login with biometrics",
                    "Please use your biometric to verify your identity",
                    "Cancel",
                    cancellationSignal,
                    bioMPCallbacks);
        }
    }
    //endregion

    //region SecurePinPad Logic

    /**
     * Show the keypad as view inside a frame layout.
     */
    private void showKeypadBuilder() {
        switch (mSettings.getDisplayMode()) {
            case Dialog: {
                showSecureKeypadDialog();
                break;
            }

            case CustomFullscreen: {
                FrameLayout container = findViewById(R.id.spp_container);
                container.setVisibility(View.VISIBLE);
                FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                if (mCustomKeyPadFragment == null) {
                    mCustomKeyPadFragment = SecurePinpadCustomFullScreenFragment.newInstance(mSettings);
                }
                fragmentTransaction.replace(container.getId(), mCustomKeyPadFragment);
                fragmentTransaction.commit();

                mOtpMainScreen.setVisibility(View.GONE);
                break;
            }

            case Fullscreen: {
                showSecureKeypadDefaultFullScreen();
                break;
            }

            default:
                break;
        }
    }

    /**
     * Display is as a Dialog. As a dialog, you can set the height and
     * width of the dialog as a ratio of the screen dimension
     */
    private void showSecureKeypadDialog() {
        // Get a SecureInputBuildV2 instance
        pinPadBuilderV2 = getSecureInputBuilder();

        pinPadBuilderV2.setMaximumAndMinimumInputLength(pinLength, pinLength);

        pinPadBuilderV2.showTopScreen(true);
        if (mSettings.isSwapControlButtons()) {
            pinPadBuilderV2.swapOkAndDeleteButton();
        }
        pinPadBuilderV2.setButtonPressVisibility(mSettings.isVisualizeButtonPress());

        if ((mSettings.isVisualizeButtonPress()) && (mSettings.isTouchRippleSetting())) {
            pinPadBuilderV2.setButtonTouchVisualEffect(SecureInputBuilderV2.ButtonTouchVisual.RIPPLE);
            pinPadBuilderV2.setRippleEffectParameters(Color.BLUE, 100);
        }

        //set input filed border color
        pinPadBuilderV2.setInputFieldBorderColor(
                SecureInputBuilderV2.UiControlFocusState.FOCUSED, Color.parseColor("#FFFFFF00"));
        pinPadBuilderV2.setInputFieldBorderColor(
                SecureInputBuilderV2.UiControlFocusState.UNFOCUSED, Color.parseColor("#FF00FFFF"));

        // Show scramble
        final boolean isScrambled = mSettings.isScrambleKeys();

        // Show dialog mode
        final boolean isDialog = true;

        // Show double pwd mode
        boolean isDoublePassword = false;
        switch (pinPadBehavior) {
            case GENERATE_OTP:
            case UPGRADE_TOKEN: {
                isDoublePassword = false;
                pinPadBuilderV2.setFirstLabel(firstLabelText);
                break;
            }
            case VERIFY_PIN: {
                isDoublePassword = false;
                pinPadBuilderV2.setFirstLabel("Enter current PIN");
                break;
            }
            case CHANGE_PIN: {
                isDoublePassword = true;
                pinPadBuilderV2.setFirstLabel("Please set new PIN:");
                pinPadBuilderV2.setSecondLabel("Confirm new PIN:");
                break;
            }
        }
        pinPadBuilderV2.setDeleteButtonText(getString(R.string.delete_button_text));
        //endregion

        //region Create fragment to show SecurePinpad
        /* The dialogFragment. */
        mSecureInputUi = pinPadBuilderV2.buildPinpad(isScrambled, isDoublePassword, isDialog,
                OtpExampleActivity.this);
        mDialogFragment = mSecureInputUi.getDialogFragment();
        mDialogFragment.show(mFragmentManager, "SECURE PIN");
        //endregion
    }

    /**
     * Display is as a Dialog. As a dialog, you can set the height and
     * width of the dialog as a ratio of the screen dimension
     */
    private void showSecureKeypadDefaultFullScreen() {

        // Get a SecureInputBuildV2 instance
        pinPadBuilderV2 = getSecureInputBuilder();

        pinPadBuilderV2.setMaximumAndMinimumInputLength(pinLength, pinLength);

        // Display secure keypad default top screen
        pinPadBuilderV2.showTopScreen(true);
        if (mSettings.isSwapControlButtons()) {
            pinPadBuilderV2.swapOkAndDeleteButton();
        }
        pinPadBuilderV2.setButtonPressVisibility(mSettings.isVisualizeButtonPress());

        if ((mSettings.isVisualizeButtonPress()) && (mSettings.isTouchRippleSetting())) {
            pinPadBuilderV2.setButtonTouchVisualEffect(SecureInputBuilderV2.ButtonTouchVisual.RIPPLE);
            pinPadBuilderV2.setRippleEffectParameters(Color.BLUE, 100);
        }

        // Show scramble
        final boolean isScrambled = mSettings.isScrambleKeys();

        // Show dialog mode
        final boolean isDialog = false;

        // Show double pwd mode
        boolean isDoublePassword = false;
        switch (pinPadBehavior) {
            case GENERATE_OTP:
            case UPGRADE_TOKEN: {
                isDoublePassword = false;
                pinPadBuilderV2.setFirstLabel(firstLabelText);
                break;
            }

            case VERIFY_PIN: {
                isDoublePassword = false;
                pinPadBuilderV2.setFirstLabel("Enter current PIN");
                break;
            }

            case CHANGE_PIN: {
                isDoublePassword = true;
                pinPadBuilderV2.setFirstLabel("Please set new PIN:");
                pinPadBuilderV2.setSecondLabel("Confirm new PIN:");
                break;
            }

            default:
                break;
        }
        pinPadBuilderV2.setDeleteButtonText(getString(R.string.delete_button_text));

        // Update keypad height and width.
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        pinPadBuilderV2.setKeypadHeight(height);
        pinPadBuilderV2.setKeypadWidth(width);

        //4.6 new APIs
        pinPadBuilderV2.setKeypadFrameColor(Color.GREEN);
        pinPadBuilderV2.setKeypadGridGradientColors(Color.BLACK, Color.BLUE);
        pinPadBuilderV2.setIsDeleteButtonAlwaysEnabled(true);

        //4.8 new APIs
        pinPadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.LTGRAY, Color.WHITE);
        pinPadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinPadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.LTGRAY);

        pinPadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.DKGRAY, Color.WHITE);
        pinPadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinPadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.DKGRAY);

        pinPadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.DKGRAY, Color.WHITE);
        pinPadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinPadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.DKGRAY);

        mSecureInputUi = pinPadBuilderV2.buildPinpad(isScrambled, isDoublePassword, isDialog,
                OtpExampleActivity.this);

        //5.0 new APIs
        pinPadBuilderV2.validateKeypadConfiguration();

        /* Frame Layout to hold the Keypad fragment. */
        FrameLayout container = findViewById(R.id.spp_container);
        container.setVisibility(View.VISIBLE);

        mDialogFragment = mSecureInputUi.getDialogFragment();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(container.getId(), mDialogFragment);
        fragmentTransaction.commit();

        mOtpMainScreen.setVisibility(View.GONE);
    }

    private SecureInputBuilderV2 getSecureInputBuilder() {

        // Create UiModule. It's the entry point for all UI relate features.
        UiModule uiModule = UiModule.create();
        // Create SecureInputService. It's the entry point for Secure input.
        SecureInputService secureInputService = SecureInputService.create(uiModule);

        // SecureInputBuildervV2 object
        SecureInputBuilderV2 pinpadBuilderV2;
        // Get a SecureInputBuilderV2 instance.
        pinpadBuilderV2 = secureInputService.getSecureInputBuilderV2();

        return pinpadBuilderV2;
    }

    /**
     * Display dialog to enter the verifyCode
     */
    @SuppressWarnings("InflateParams")
    private void showVerifyCodeDialog() {
        LayoutInflater inflater = LayoutInflater.from(OtpExampleActivity.this);
        View dialogLayout = inflater.inflate(R.layout.verifycode_input_dialog, null);

        final EditText verifyCodeEditText = dialogLayout.findViewById(R.id.txt_code);
        AlertDialog.Builder builder = new AlertDialog.Builder(OtpExampleActivity.this);
        builder.setView(dialogLayout);
        builder.setTitle("Verify Code");
        builder.setPositiveButton(getString(R.string.ok_button_text),
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Tools.hideKeyboard(context, verifyCodeEditText);

                        if (verifyCodeEditText.getText().toString().length() == 8) {
                            verifyCode = core.getSecureContainerFactory()
                                    .fromString(verifyCodeEditText.getText().toString());

                            if (!mSettings.isEnableSecurePinPad()) {
                                showRawPinInput();
                            } else {
                                // Try with the securepinpad, ActivationException will be thrown if the feature is not activated by the given ActivationCode.
                                try {
                                    pinPadBuilderV2 = getSecureInputBuilder();
                                    showKeypadBuilder();
                                } catch (ActivationException e) {
                                    logTextView.append("\tSecure Pin Pad is not enabled.\n");
                                }
                            }
                        } else {
                            logTextView.append("\tThe verify code must be 8 digits long. Please enter again...\n");
                        }
                    }
                })
                .setCancelable(false)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Tools.hideKeyboard(context, verifyCodeEditText);
                                dialog.cancel();
                            }
                        });

        builder.create().show();

    }
    //endregion

    //region SecurePinPad callback

    /**
     * Called when the user press the keypad button and changes the number of currently typed PIN
     * digits so that the application could update UI accordingly.
     */
    @Override
    public void onKeyPressedCountChanged(int newCount, int inputField) {

        if ((newCount == pinLength) && (mSettings.isSimulateOkButtonPress())) {
            mSecureInputUi.simulateOkButtonPress();
        }
        // When default top screen is used, implementation could be skipped for this callback
    }

    @Override
    public void onInputFieldSelected(int i) {
        // When default top screen is used, implementation could be skipped for this callback
    }

    @Override
    public void onOkButtonPressed() {
        Toast.makeText(this, getString(R.string.ok_button_text) + " clicked.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteButtonPressed() {
        Toast.makeText(this, getString(R.string.delete_button_text) + " clicked.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String var1) {
        Toast.makeText(this, "Error from Ezio SecureInput: " + var1, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFinish(final PinAuthInput pin1, final PinAuthInput pin2) {
        if (pinPadBuilderV2 != null) {
            pinPadBuilderV2.wipe();
        }
        // Security Guideline: AND08. Secure Pin Pad
        if (mSettings.getDisplayMode() == OtpSettings.ENU_DISPLAY_MODE.Dialog) {
            dismissPinPadDialog();
        } else {
            // dismiss keypad, user can write your own dismiss method. User has to
            // dismiss it since the dialogFragment cannot be reused.
            // To user it, you have to create a new dialogFragment from pinpad
            // object.
            dismissKeyPad();
        }
        processSecurePinPadResult(pin1, pin2);
    }
    //endregion

    //region SecurePinPad Utilities
    @Override
    public void onCustomFullScreenFinish(PinAuthInput pin1, PinAuthInput pin2) {
        dismissCustomKeyPad();

        processSecurePinPadResult(pin1, pin2);
    }

    private void processSecurePinPadResult(PinAuthInput pin1, PinAuthInput pin2) {
        mOtpMainScreen.setVisibility(View.VISIBLE);
        // Save a temporary password backup since we need it for PIN change later
        currentPin = pin1.clone();

        switch (pinPadBehavior) {
            case GENERATE_OTP: {
                // Reset SecurePinPadBehavior
                pinPadBehavior = PinPadBehavior.UNDEFINED;

                // This example places all SDK examples in a background task. Typically,
                // only operations noted as requiring significantly execution time (e.g.
                // TokenManager.createToken) need to execute in a background task.
                switch (mSettings.getOtpType()) {
                    case TOTP:
                        GetTotpOtpTask getTotpOtpTask = new GetTotpOtpTask(pin1, logTextView);
                        getTotpOtpTask.execute();
                        break;
                    case CAP:
                        GetCapOtpTask getCapOtpTask = new GetCapOtpTask(pin1, logTextView);
                        getCapOtpTask.execute();
                        break;
                    case VIC:
                        VicVerifyTask vicVerifyTask = new VicVerifyTask(verifyCode, pin1, logTextView);
                        vicVerifyTask.execute();
                        break;
                    default:
                        break;
                }

                break;
            }

            case VERIFY_PIN: {
                // Reset SecurePinPadBehavior
                pinPadBehavior = PinPadBehavior.UNDEFINED;


                //start pin pad again to change pin
                try {
                    // Define secure pin pad behavior
                    pinPadBehavior = PinPadBehavior.CHANGE_PIN;
                    if (!mSettings.isEnableSecurePinPad()) {
                        showRawPinChangePin();
                    } else {
                        mSettings.setDoublePasswordMode(true);
                        showKeypadBuilder();
                    }
                } catch (ActivationException e) {
                    logTextView.append("\tSecure Pin Pad is not enabled.\n");
                }

                break;
            }

            case CHANGE_PIN: {
                // Reset SecurePinPadBehavior
                pinPadBehavior = PinPadBehavior.UNDEFINED;

                PinAuthInput oldPin, newPin;

                if (!pin1.equals(pin2)) {
                    currentPin.wipe();
                    logTextView.append("\tThe new PIN you entered must be consistent.\n");
                    break;
                }

                oldPin = currentPin;
                newPin = pin1;

                try {
                    switch (mSettings.getOtpType()) {
                        case TOTP:
                            oathToken.changePin(oldPin, newPin);
                            logTextView.append("\tNew PIN changed for token " + FakeUi.selectTokenName(oathService) + "\n");
                            break;
//                        case CAP:
//                            capToken.changePin(oldPin, newPin);
//                            logTextView.append("\tNew PIN changed for token " + FakeUi.selectTokenName(capService) + "\n");
//                            break;
//                        case VIC:
//                            vicToken.changePin(oldPin, newPin);
//                            logTextView.append("\tNew PIN changed for token " + FakeUi.selectTokenName(vicService) + "\n");
//                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    if (e instanceof PinRuleException) {
                        getPinRuleErrorWithCode((PinRuleException) e, logTextView);
                    } else {
                        logTextView.append("\tUpgrade token failed with exception: " + e.getClass().getSimpleName() +
                                "\n\tMessage: " + e.getMessage() + "\n");
                    }
                } finally {
                    //GEN08.PIN sanitization
                    if (currentPin != null)
                        currentPin.wipe();
                    if (oldPin != null)
                        oldPin.wipe();
                    if (newPin != null)
                        newPin.wipe();
                }
                break;
            }

            case UPGRADE_TOKEN: {
                // Reset SecurePinPadBehavior
                pinPadBehavior = PinPadBehavior.UNDEFINED;

                try {
                    boolean tokenUpgraded = false;
                    switch (mSettings.getOtpType()) {
                        case TOTP: {
                            if (!oathToken.isMultiAuthModeEnabled()) {
                                oathToken.upgradeToMultiAuthMode(pin1);
                                logTextView.append("\tThe token has been upgraded to Bio Fingerprint Mode\n");
                            }

                            if (oathToken.isMultiAuthModeEnabled() && !oathToken.isAuthModeActive(bioAuthMode)) {
                                oathToken.activateAuthMode(bioAuthMode, pin1);
                                logTextView.append("Token:" + oathToken.getName() + "\n\t Enabled for Bio Fingerprint Mode\n");
                                tokenUpgraded = true;
                            }
                            break;
                        }

//                        case CAP: {
//                            if (!capToken.isMultiAuthModeEnabled()) {
//                                capToken.upgradeToMultiAuthMode(pin1);
//                                logTextView.append("\tThe token has been upgraded to Bio Fingerprint Mode\n");
//                            }
//
//                            if (capToken.isMultiAuthModeEnabled() && !capToken.isAuthModeActive(bioAuthMode)) {
//                                capToken.activateAuthMode(bioAuthMode, pin1);
//                                logTextView.append("Token:" + capToken.getName() + "\n\t Enabled for Bio Fingerprint Mode\n");
//                                tokenUpgraded = true;
//                            }
//                            break;
//                        }
//
//                        case VIC: {
//                            if (!vicToken.isMultiAuthModeEnabled()) {
//                                vicToken.upgradeToMultiAuthMode(pin1);
//                                logTextView.append("\tThe token has been upgraded to Bio Fingerprint Mode\n");
//                            }
//
//                            if (vicToken.isMultiAuthModeEnabled() && !vicToken.isAuthModeActive(bioAuthMode)) {
//                                vicToken.activateAuthMode(bioAuthMode, pin1);
//                                logTextView.append("Token:" + vicToken.getName() + "\n\t Enabled for Bio Fingerprint Mode\n");
//                                tokenUpgraded = true;
//                            }
//                            break;
//                        }

                        default:
                            break;
                    }

                    if (tokenUpgraded) {
                        getOtpBiometricBtn.setEnabled(true);
                        deactivateBiometricBtn.setEnabled(true);
                    }
                } catch (IdpException e) {
                    logTextView.append("\tUpgrade token failed with exception: " + e.getClass().getSimpleName() +
                            "\n\tMessage: " + e.getMessage() + "\n");
                } finally {
                    // Security Guideline: GEN08. PIN sanitization
                    // The PIN must be wiped ASAP
                    if (pin1 != null) {
                        pin1.wipe();
                    }
                    activateBioMetricBtn.setEnabled(false);
                }
                break;
            }

            default:
                break;
        }
    }

    private void getPinRuleErrorWithCode(PinRuleException ex, TextView logTextView) {
        String er = "";
        if (ex.getOffendingPinRule().equals(new PinRuleUniform())) {
//            callBack.onFail(26, "PIN mới phải tuân theo định dạng sau: Không cho phép 04 số liền nhau (ví dụ: 1111)", null);
            er = ("PIN mới phải tuân theo định dạng sau: Không cho phép 04 số liền nhau (ví dụ: 1111)");
        } else if (ex.getOffendingPinRule().equals(new PinRuleIdentical())) {
//            callBack.onFail(24, "PIN mới phải tuân theo định dạng sau: Không trùng PIN cũ", null);
            er = "PIN mới phải tuân theo định dạng sau: Không trùng PIN cũ";
        } else if (ex.getOffendingPinRule().equals(new PinRuleSeries())) {
//            callBack.onFail(22, "PIN mới phải tuân theo định dạng sau: Không cho phép 04 số liền nhau (ví dụ: 1234)", null);
            er = "PIN mới phải tuân theo định dạng sau: Không cho phép 04 số liền nhau (ví dụ: 1234)";
        } else if (ex.getOffendingPinRule().equals(new PinRulePalindrome())) {
//             callBack.onFail(27, "The pin is a palindrome (e.g. 12321)", null);
            er = "The pin is a palindrome (e.g. 12321)";
        }
//        else if(ex.getOffendingPinRule().equals(new PinRuleLength()))
//            failWith(-1, "PIN mới phải tuân theo định dạng sau: Không cho phép 04 số liền nhau (ví dụ: 1234)");
        else {
//            callBack.onFail(-1, ex.getOffendingPinRule().toString(), null);
            er = ex.getOffendingPinRule().toString();
        }

        logTextView.append("\tUpgrade token failed with exception: " + ex.getClass().getSimpleName() +
                "\n\tMessage: " + er + "\n");
    }


    private void dismissPinPadDialog() {
        if (mDialogFragment != null && mDialogFragment.getDialog() != null
                && mDialogFragment.getDialog().isShowing()) {
            mDialogFragment.dismiss();
            mDialogFragment = null;
        }
    }

    /**
     * dismiss the keypad. The application maker can write their own method to
     * dismiss the keypad.
     */
    private boolean dismissKeyPad() {
        boolean processed = false;

        // if it is displayed as a dialog
        if (mDialogFragment != null) {
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.remove(mDialogFragment);
            fragmentTransaction.commitAllowingStateLoss();

            mOtpMainScreen.setVisibility(View.VISIBLE);
            mDialogFragment = null;

            processed = true;
        }

        return processed;
    }

    /**
     * dismiss the Custom keypad. The application maker can write their own method to
     * dismiss the keypad.
     */
    private boolean dismissCustomKeyPad() {
        boolean processed = false;

        // if it is displayed as a view
        if (mCustomKeyPadFragment != null) {
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.remove(mCustomKeyPadFragment);
            fragmentTransaction.commit();

            mOtpMainScreen.setVisibility(View.VISIBLE);
            mCustomKeyPadFragment = null;

            processed = true;
        }

        return processed;
    }

    //endregion
}