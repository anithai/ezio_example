/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob.util;

import android.os.Handler;
import android.os.Looper;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.oob.OobExampleActivity;
import com.gemalto.ezio.mobile.sdk.example.oob.message.OobMessageAdapter;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.oob.OobManager;
import com.gemalto.idp.mobile.oob.OobMessageResponse;
import com.gemalto.idp.mobile.oob.OobResponse;
import com.gemalto.idp.mobile.oob.message.OobAcknowledgeCallback;
import com.gemalto.idp.mobile.oob.message.OobErrorMessage;
import com.gemalto.idp.mobile.oob.message.OobFetchMessageCallback;
import com.gemalto.idp.mobile.oob.message.OobFetchMessageResponse;
import com.gemalto.idp.mobile.oob.message.OobIncomingMessage;
import com.gemalto.idp.mobile.oob.message.OobMessageManager;
import com.gemalto.idp.mobile.oob.message.OobProviderToUserMessage;
import com.gemalto.idp.mobile.oob.message.OobSendMessageCallback;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyRequest;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyResponse;
import com.gemalto.idp.mobile.oob.message.OobUserMessageAttachment;
import com.gemalto.idp.mobile.oob.message.OobUserToProviderMessage;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OobMessagingHelper {
    private static OobMessagingHelper instance = null;
    private OobExampleActivity activity;
    private OobManager oobManager;
    private OobMessageAdapter adapter;

    private OobMessagingHelper(OobExampleActivity activity, OobMessageAdapter adapter, OobManager oobManager) {
        this.activity = activity;
        this.oobManager = oobManager;
        this.adapter = adapter;
    }

    public static OobMessagingHelper initialize(OobExampleActivity activity, OobMessageAdapter adapter, OobManager oobManager) {
        if (instance == null) {
            instance = new OobMessagingHelper(activity, adapter, oobManager);
        }
        return instance;
    }

    public static OobMessagingHelper getInstance() {
        return instance;
    }

    /**
     * Fetch message
     *
     * @param clientId
     */
    public void fetchMessage(String clientId) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        activity.showProgressBar();

        mOobMessageManager.fetchMessage(
                15000, // fetch with timeout value
                new OobFetchMessageCallback() {
                    @Override
                    public void onFetchMessageResult(OobFetchMessageResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            final OobIncomingMessage message = response.getOobIncomingMessage();

                            if (message != null) {
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.addMessage(message);

                                    }
                                });
                            } else {
                                activity.appendLog("\t No message found!\n");
                            }
                        } else {
                            // handle the error
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }

    /**
     * Fetch message
     *
     * @param clientId
     */
    public void fetchMessageById(String clientId, String messageId) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        activity.showProgressBar();

        mOobMessageManager.fetchMessage(
                messageId, // fetch with messageId(ie. obtained from FCM)
                new OobFetchMessageCallback() {
                    @Override
                    public void onFetchMessageResult(OobFetchMessageResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            final OobIncomingMessage message = response.getOobIncomingMessage();

                            if (message != null) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter.addMessage(message);
                                    }
                                });
                            } else {
                                activity.appendLog("\t No message found!\n");
                            }
                        } else {
                            // handle the error
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }

    /**
     * Acknowledge a message when needed
     *
     * @param clientId
     * @param message
     */
    public void acknowledgeMessage(String clientId, final OobIncomingMessage message) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        activity.showProgressBar();

        mOobMessageManager.acknowledgeMessage(
                message,
                new OobAcknowledgeCallback() {

                    @Override
                    public void onAcknowledgeResult(OobResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.removeMessage(message);
                                }
                            });

                            activity.appendLog("\t Message acknowledged \n");
                        } else {
                            // handle error response
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }

    /**
     * explicitly create error response
     *
     * @param clientId
     * @param message
     */
    public void createErrorResponse(String clientId, final OobIncomingMessage message) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        Map<String, String> META = new HashMap<>();

        // Create error from incoming message
        // Or create it from messageManager
        OobErrorMessage oobErrorMessage = message.createOobError(
                OobErrorMessage.ERROR_CODE_CUSTOM_BASE, // or other integer defined between client and provider
                "description",
                "stackTrace",
                "externalRef",
                META);

        activity.showProgressBar();

        mOobMessageManager.sendMessage(
                oobErrorMessage,
                new OobSendMessageCallback() {

                    @Override
                    public void onSendMessageResult(OobMessageResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            activity.appendLog("\t Error report sent \n");
                        } else {
                            // handle error response
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }

    /**
     * Send reply to provider to user message
     *
     * @param clientId
     * @param message
     * @param replyTitle
     * @param replyContent
     */
    public void sendReplyUserMessage(
            String clientId,
            final OobProviderToUserMessage message, String
                    replyTitle, String replyContent) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        Map<String, String> META = new HashMap<>();
        // Create your attachment
        OobUserMessageAttachment[] ATTA = new OobUserMessageAttachment[1];
        ATTA[0] = mOobMessageManager.createUserMessageAttachment(
                IdpCore.getInstance().getSecureContainerFactory().fromString(replyTitle), // Subject
                "message/vnd.gemalto.ezio.oob.UserMessage_1.0+json", // ContentType
                IdpCore.getInstance().getSecureContainerFactory().createByteArray(replyContent.getBytes(), true), // Content
                META);

        activity.showProgressBar();

        // Create outgoing message from incoming message
        // Or create it from messageManager
        OobUserToProviderMessage outgoingMessage = message.createResponse(
                "SG", // Locale
                new Date(), // Date
                IdpCore.getInstance().getSecureContainerFactory().fromString(replyTitle), // Subject
                "From", // From
                "message/vnd.gemalto.ezio.oob.UserMessage_1.0+json", // or defined your own type between application and backend
                IdpCore.getInstance().getSecureContainerFactory().createByteArray(replyContent.getBytes(), true), // Content
                ATTA, // Attachment objects
                META); // Meta

        mOobMessageManager.sendMessage(
                outgoingMessage,
                new OobSendMessageCallback() {

                    @Override
                    public void onSendMessageResult(OobMessageResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.removeMessage(message);
                                }
                            });

                            activity.appendLog("\t User message sent \n");
                        } else {
                            // handle error response
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }

    /**
     * handle transaction verification request
     *
     * @param clientId
     * @param message
     * @param value    Accept or Deny
     */
    public void handleTransactionVerification(
            String clientId,
            final OobTransactionVerifyRequest message,
            final OobTransactionVerifyResponse.OobTransactionVerifyResponseValue value) {

        // create OobMessageManager object
        OobMessageManager mOobMessageManager = oobManager.getOobMessageManager(clientId, FakeAppData.getOobProviderId());

        Map<String, String> META = new HashMap<>();

        // Create the response from incoming request object
        // Or create it from messageManager
        OobTransactionVerifyResponse oobTransactionVerifyResponse = message.createResponse(value, META);

        activity.showProgressBar();

        String action = "Accepting transaction...";
        if (value == OobTransactionVerifyResponse.OobTransactionVerifyResponseValue.REJECTED) {
            action = "Rejecting transaction...";
        }

        mOobMessageManager.sendMessage(
                oobTransactionVerifyResponse,
                new OobSendMessageCallback() {

                    @Override
                    public void onSendMessageResult(final OobMessageResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.removeMessage(message);
                                }
                            });

                            activity.appendLog("\t Transaction response sent \n");
                        } else {
                            // handle error response
                            activity.handleErrorResponse(response);
                        }
                    }
                });
    }
}
