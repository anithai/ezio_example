/**
 *
 *   $Rev::                     $:  Revision of last commit
 *   $Author::                  $:  Author of last commit
 *   $Date::                    $:  Date of last commit
 *
 *   Original author: 
 *   Date: 
 *   Purpose: see documentation block below 
 *   Warning: see documentation block below 
 *   
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c)       GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 * 
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.util.net;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Http {

    public static class PostData {
        private StringBuffer data = new StringBuffer();
        private boolean first = true;

        public PostData() {
        }

        public PostData add(String s) {
            data.append(s);
            return this;
        }

        public PostData add(String key, String value) throws UnsupportedEncodingException {
            if (!first) {
                data.append("&");
            } else {
                first = false;
            }

            final String format = "UTF-8";
            data.append(URLEncoder.encode(key, format));
            data.append("=");
            data.append(URLEncoder.encode(value, format));

            return this;
        }

        public String toString() {
            return data.toString();
        }
    }
}
