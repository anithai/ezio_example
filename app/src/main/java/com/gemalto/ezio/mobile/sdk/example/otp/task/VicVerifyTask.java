/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp.task;

import android.os.AsyncTask;
import android.text.Html;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeUi;
import com.gemalto.idp.mobile.authentication.AuthInput;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManager;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManagerException;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.otp.OtpModule;
import com.gemalto.idp.mobile.otp.vic.VicDevice;
import com.gemalto.idp.mobile.otp.vic.VicService;
import com.gemalto.idp.mobile.otp.vic.VicToken;

import java.lang.ref.WeakReference;


public class VicVerifyTask extends AsyncTask<Void, CharSequence, Void> {

    // The entry point for the VIC features
    private VicService vicService;
    private String tokenName;
    // The text view to update with the results
    private WeakReference<TextView> viewToUpdate;
    private String currentTaskName;

    private AuthInput authInput;
    private SecureString verifyCode;

    public VicVerifyTask(SecureString verifyCode,
                         AuthInput authInput,
                         TextView logView) {
        // Create OtpModule. It's the entry point for all Otp relate features.
        OtpModule otpModule = OtpModule.create();
        // Create CapService. It's the entry point for all Cap relate features.
        this.vicService = VicService.create(otpModule);
        this.tokenName = FakeUi.selectTokenName(this.vicService);

        this.verifyCode = verifyCode;
        this.authInput = authInput;
        this.viewToUpdate = new WeakReference<>(logView);
    }

    /**
     * This method starts the example logic but does not serve as any kind of
     * example for the Ezio Mobile SDK.
     */
    @Override
    protected Void doInBackground(Void... params) {
        currentTaskName = "Verify VIC";
        verifyVic();
        return null;
    }

    protected void onProgressUpdate(CharSequence... textToAppend) {
        TextView view = viewToUpdate.get();
        if (view != null) {
            view.append(Html.fromHtml("<b><font color=\"#00FFFF\">" + currentTaskName + ": " + "</font></b>"));
            view.append("\n");

            for (CharSequence cs : textToAppend) {
                view.append("\t" + cs + "\n");
            }
        }
    }

    /**
     * This method will verify a VIC. It provides a real world example (minus
     * the bad UI) of how this should be performed and it includes all security
     * guidelines that must be followed.
     */
    private void verifyVic() {
        try {
            // VERIFY A VIC

            // The same custom fingerprint data used to create the token must
            // be provided before retrieving the token.
            byte[] fingerprintData = FakeAppData.getCustomFingerprintData();

            // Notice that the fingerprint is included when creating the
            // token.
            // Internally, it is still a CapToken for VicDevice
            VicToken token = vicService.getTokenManager().getToken(tokenName,
                    fingerprintData);

            // A VicDevice is the object that verifies VIC codes.
            // Notice it has no ability to generate cap otps.
            VicDevice device = vicService.getFactory().createVicDevice(token);

            if (device.isVerifyLastAttempt()) {
                // Show the user warning message that this is the last
                // attempt.
            }
            if (device.isVerifyBlocked()) {
                // Show to user the verification function is blocked, please
                // contact the bank.
            }

            // pin = FakeUi.promptForPin(emc);
            boolean isVerified = device.verifyIssuerCode(
                    verifyCode, authInput);

            // In this dummy example, isVerified will always be false.
            publishProgress("Current token used: " + tokenName + "\n\t" + "VIC Token isVerified = "
                    + isVerified);
        } catch (PasswordManagerException e) {
            publishProgress("\tPassword Manager login Exception.\n");
        } catch (Exception e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
            e.printStackTrace();
        } finally {
            // Security Guideline: GEN08. PIN sanitization
            // The PIN must be wiped ASAP
            if (authInput != null) {
                authInput.wipe();
            }
            // The vic must also be wiped ASAP
            verifyCode.wipe();
        }
    }
}
