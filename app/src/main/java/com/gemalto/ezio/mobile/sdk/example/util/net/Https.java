/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2016  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.util.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Https {

    private static void disableSSLChecking(HttpsURLConnection conn) throws NoSuchAlgorithmException, KeyManagementException {
        // Use TLS for Android 4.1 & below
        // TLSv1.2 is only implemented on Android from 4.1 onwards.
        SSLContext sc = SSLContext.getInstance("TLS");
        TrustManager tm = new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };
        sc.init(null, new TrustManager[] { tm }, null);
        conn.setSSLSocketFactory(sc.getSocketFactory());
        
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        conn.setHostnameVerifier(allHostsValid);
    }

    public static String post(String url, String data) throws IOException {
        return post(url, data, "application/x-www-form-urlencoded");
    }

    public static String post(String url, String data, String contentType) throws IOException {
        StringBuffer response;

        HttpsURLConnection conn = (HttpsURLConnection) new URL(url).openConnection();
        try {
            disableSSLChecking(conn);
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (KeyManagementException ex) {
            ex.printStackTrace();
        }
        try {
            // debug("POST (" + url + "): ");

            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Length", data.length() + "");
            conn.setRequestProperty("Content-Type", contentType);

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            if (conn.getResponseCode() != 200) {
                String error = conn.getResponseMessage();
                throw new RuntimeException("HTTP " + conn.getResponseCode() + ": " + error);
            }

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            try {

                response = new StringBuffer();
                String line;
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
            } finally {
                rd.close();
            }
        } finally {
            conn.disconnect();
        }

        // debug("response: " + response);
        return (response == null) ? null : response.toString();
    }

    // This function is used to print log for debug purpose, currently not being
    // used.
    @SuppressWarnings("unused")
    private static void debug(String string) {
        final boolean on = true;// false;
        if (on) {
            System.out.println(string);
        }
    }
}
