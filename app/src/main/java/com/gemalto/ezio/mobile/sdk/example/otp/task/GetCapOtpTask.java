/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp.task;

import android.os.AsyncTask;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeUi;
import com.gemalto.ezio.mobile.sdk.example.otp.OtpSettings;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.authentication.AuthInput;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.IdpException;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManager;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.otp.OtpModule;
import com.gemalto.idp.mobile.otp.cap.CapDevice;
import com.gemalto.idp.mobile.otp.cap.CapService;
import com.gemalto.idp.mobile.otp.cap.CapToken;
import com.gemalto.idp.mobile.otp.oath.OathDevice;
import com.gemalto.idp.mobile.otp.oath.OathService;
import com.gemalto.idp.mobile.otp.oath.OathToken;

import java.lang.ref.WeakReference;

public class GetCapOtpTask extends AsyncTask<Void, CharSequence, Void> {

    // The text view to update with the results
    private WeakReference<TextView> viewToUpdate;
    private String currentTaskName;
    private AuthInput authInput;
    private CapService capService;
    private String tokenName;

    public GetCapOtpTask(AuthInput authInput, TextView viewToUpdate) {
        this.authInput = authInput;

        // Create OtpModule. It's the entry point for all Otp relate features.
        OtpModule otpModule = OtpModule.create();
        // Create CapService. It's the entry point for all Cap relate features.
        this.capService = CapService.create(otpModule);

        this.tokenName = FakeUi.selectTokenName(this.capService);

        this.viewToUpdate = new WeakReference<>(viewToUpdate);
    }

    /**
     * This method starts the example logic but does not serve as any kind of
     * example for the Ezio Mobile SDK.
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            // LIST TOKENS AND GENERATE OTPs
            // Note: For a real application, listing tokens and generating OTPs
            // is fast enough to be done in the UI thread. The application may
            // decide if this is done in a separate execution thread.
            currentTaskName = "Generate OTP";

            if (authInput == null) publishProgress("Pin is null!");
            generateOtps();
        } catch (Exception e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(CharSequence... textToAppend) {
        TextView txtView = viewToUpdate.get();
        if (txtView != null) {
            txtView.append(Tools.generateLogTitle(currentTaskName));

            for (CharSequence cs : textToAppend) {
                txtView.append("\t" + cs + "\n");
            }
        }
    }

    /**
     * This method will generate a OATH TOTP for all the tokens provisioned
     * in the device. It provides a real world example (minus the bad UI) of
     * how this should be performed and it includes all security guidelines that
     * must be followed.
     */
    private void generateOtps() {
        SecureString otp = null;

        try {
            // The same custom fingerprint data used to create the token must
            // be provided before retrieving the token.
            byte[] fingerprintData = FakeAppData.getCustomFingerprintData();

            // GENERATE CAP MODE 2 OTP

            // Notice that the fingerprint is included when creating the token.
            CapToken capToken = capService.getTokenManager().getToken(tokenName,
                    fingerprintData);

            // A device is the object that generates OTPs. Notice this will
            // create CAP OTPs.
            // WARNING: the following line of code creates a CapDevice with default cap setting.
            // This setting has to be synchronized with your backend Authentication Server.
            // Or you will have to call CapFactory.createSoftCapDevice(token, settings),
            // Whereas settings can be created by:
            // MutableSoftCapSettings settings = CapFactory.createSoftCapSettings();
            // settings.setCdol(arg0);
            // settings.setIpb(arg0);
            // ...
            CapDevice capDevice = capService.getFactory().createCapDevice(capToken);


            // For the list of CAP Modes & its differences, refer to chapter
            // 'Chip Authentication Program (CAP) Service' of the Programmer's Guide.
            otp = capDevice.getOtpMode2(authInput);

            // Security Guideline: AND01. Sensitive data leaks
            // The OTP will remain displayed in the UI even after this
            // local
            // variable is wiped. The application should clear this
            // information as soon as it is consumed by the user (in
            // this example's case, until the app is paused).
            publishProgress("Current token used: " + tokenName + "\n\t" +
                    "OTP Type: CAP " + " OTP = " + otp + "\n\t" +
                    "Last token is taken from the database. " +
                    "Please modify the code if you wish to select among different tokens.\n\t");
         } catch (IdpException e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
            e.printStackTrace();
        } finally {
            // Security Guideline: GEN12. Wiping assets
            // The OTP must be wiped after use.
            if (otp != null) {
                otp.wipe();
            }

            // Security Guideline: GEN08. PIN sanitization
            // The PIN must be wiped ASAP
            if (authInput != null) {
                authInput.wipe();
            }
        }
    }
}
