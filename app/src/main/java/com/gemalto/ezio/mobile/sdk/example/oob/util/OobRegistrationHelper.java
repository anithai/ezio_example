/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob.util;

import android.content.SharedPreferences;

import com.gemalto.ezio.mobile.sdk.example.oob.OobExampleActivity;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.oob.OobManager;
import com.gemalto.idp.mobile.oob.OobResponse;
import com.gemalto.idp.mobile.oob.registration.OobRegistrationCallback;
import com.gemalto.idp.mobile.oob.registration.OobRegistrationManager;
import com.gemalto.idp.mobile.oob.registration.OobRegistrationRequest;
import com.gemalto.idp.mobile.oob.registration.OobRegistrationResponse;
import com.gemalto.idp.mobile.oob.registration.OobUnregistrationCallback;

public class OobRegistrationHelper {

    private static OobRegistrationHelper instance = null;
    private OobManager oobManager;
    private SharedPreferences pref;
    private OobExampleActivity activity;

    private OobRegistrationHelper(OobExampleActivity activity, SharedPreferences sharedPreferences, OobManager oobManager) {
        this.pref = sharedPreferences;
        this.oobManager = oobManager;
        this.activity = activity;
    }

    public static OobRegistrationHelper initialize(OobExampleActivity activity, SharedPreferences sharedPreferences, OobManager oobManager) {
        if (instance == null) {
            instance = new OobRegistrationHelper(activity, sharedPreferences, oobManager);
        }
        return instance;
    }

    /**
     * Register the client
     *
     * @param userId           userId during enrolment
     * @param userAlias        userAlias
     * @param registrationCode registrationCode obtained from the provider
     */
    public void register(String userId, String userAlias, final SecureString
            registrationCode) {

        // create OobRegistrationRequest object
        OobRegistrationRequest oobRegistrationRequest = new OobRegistrationRequest(
                userId,
                userAlias,
                OobRegistrationRequest.RegistrationMethod.REGISTRATION_CODE,
                registrationCode);

        // create OobRegistrationManager object
        OobRegistrationManager mOobRegistrationManager = oobManager.getOobRegistrationManager();

        activity.showProgressBar();

        mOobRegistrationManager.register(
                oobRegistrationRequest,
                new OobRegistrationCallback() {
                    @Override
                    public void onOobRegistrationResponse(OobRegistrationResponse response) {

                        activity.hideProgressBar();

                        if (response.isSucceeded()) {
                            final String clientId = response.getClientId();
                            pref.edit().putString(OobExampleActivity.PREF_KEY_CLIENT_ID, clientId).apply();

                            activity.appendLog("\t ClientId registered:" + clientId + "\n");
                        } else {
                            // handle the error
                            activity.handleErrorResponse(response);
                        }

                        // Security Guideline: GEN12. Wiping assets
                        // The registration code must be wiped after use.
                        registrationCode.wipe();
                    }
                });
    }

    /**
     * Un-register the client
     */
    public void unregister(final String clientId) {
        oobManager.getOobUnregistrationManager(clientId).unregister(
                new OobUnregistrationCallback() {
                    @Override
                    public void onOobUnregistrationResponse(OobResponse response) {
                        do {
                            if (!response.isSucceeded()) {
                                activity.handleErrorResponse(response);
                                break;
                            }

                            pref.edit().remove(OobExampleActivity.PREF_KEY_CLIENT_ID).apply();
                            activity.appendLog("\t ClientId un-registered:" + clientId + "\n");
                        } while (false);
                    }
                });
    }
}
