/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.msp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.root.RootDetector;
import com.gemalto.idp.mobile.msp.MspCapData;
import com.gemalto.idp.mobile.msp.MspFactory;
import com.gemalto.idp.mobile.msp.MspFrame;
import com.gemalto.idp.mobile.msp.MspModule;
import com.gemalto.idp.mobile.msp.MspOathData;
import com.gemalto.idp.mobile.msp.MspParser;
import com.gemalto.idp.mobile.msp.MspService;
import com.gemalto.idp.mobile.msp.exception.MspException;

import java.math.BigInteger;

public class MspExampleActivity extends AppCompatActivity {

    private IdpCore core;
    private Button mspParseOathButton, mspParseCapButton;
    private TextView logTextView;


    //"OCRA, OCRA-1:HOTP-SHA256-8:C-QH16S, no obfuscation, no signature"
    private String OATH_DATA = "03d684070100e509990cd127800000000052cfe87800220c10505559514748554d4541414a5055594cd408112233aabbccddee";
    //"CAP Mode 2, no obfuscation, no signature",
    private String CAP_DATA = "03d684070100a07c80b3d11d800000000052cfe87800110c10505559514748554d4541414a5055594c";

    /**
     * Called when the activity is first created.
     */
    @Override
    @SuppressWarnings("ObsoleteSdkInt")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Security Guideline: AND01. Sensitive data leaks
        // Prevents screenshots of the app
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            Tools.disableScreenShot(this);
        }

        setContentView(R.layout.activity_msp_example);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mspParseOathButton = findViewById(R.id.msp_parse_oath);
        mspParseCapButton = findViewById(R.id.msp_parse_cap);

        logTextView = findViewById(R.id.log);
        logTextView.setMovementMethod(new ScrollingMovementMethod());

        core = IdpCore.getInstance();

        // DETECTING IF A ROOTED DEVICE
        // Note: For a real application, detecting rooted devices
        // is fast enough to be done in the UI thread. The application may
        // decide if this is done in a separate execution thread.
        RootDetector detector = core.getRootDetector();
        // Is the physical device rooted?
        RootDetector.RootStatus rootStatus = detector.getRootStatus();
        if (rootStatus == RootDetector.RootStatus.ROOTED) {
            // Besides the root policy configured,
            // application can decide what to do when device is rooted
            logTextView.append("Device detected as rooted.\n");
        } else {
            logTextView.append("Device detected as not rooted.\n");
        }

        MspModule mspModule = MspModule.create();
        MspService mspService = MspService.create(mspModule);
        MspFactory mspFactory = mspService.getFactory();
        final MspParser mspParser = mspFactory.createMspParser();

        // Click Parse OATH data button to start parsing the OATH data
        mspParseOathButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                logTextView.append("\n\n\n\nParsing Data...\n\n\n");
                logTextView.append("OATH MSP Frame\n\n\n");
                try {
                    byte[] frameBuff = new BigInteger(OATH_DATA, 16).toByteArray();
                    MspFrame oathModeFrame = mspParser.parse(frameBuff);
                    MspOathData oathData = (MspOathData) mspParser.parseMspData(oathModeFrame);

                    switch (oathData.getMode()) {
                        case MspOathData.MSP_OATH_HOTP:
                            logTextView.append("The signing request mode is HOTP.\n");
                            break;
                        case MspOathData.MSP_OATH_TOTP:
                            logTextView.append("The signing request mode is TOTP.\n");
                            break;
                        case MspOathData.MSP_OATH_OCRA:
                            logTextView.append("The signing request mode is OCRA.\n");
                            break;
                        default:
                            logTextView.append("The signing request mode is wrong!\n");

                    }

                } catch (MspException e) {
                    e.printStackTrace();
                }

            }
        });

        // Click Parse CAP data button to start parsing the OATH data
        mspParseCapButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                logTextView.append("\n\n\n\nParsing Data...\n\n\n");
                logTextView.append("CAP MSP Frame\n\n\n");
                try {
                    byte[] frameBuff = new BigInteger(CAP_DATA, 16).toByteArray();
                    MspFrame capModeFrame = mspParser.parse(frameBuff);
                    MspCapData capData = (MspCapData) mspParser.parseMspData(capModeFrame);

                    switch (capData.getMode()) {
                        case MspCapData.MSP_CAP_MODE1:
                            logTextView.append("The signing request mode is CAP Mode 1.\n");
                            break;
                        case MspCapData.MSP_CAP_MODE2:
                            logTextView.append("The signing request mode is CAP Mode 2.\n");
                            break;
                        case MspCapData.MSP_CAP_MODE2_TDS:
                            logTextView.append("The signing request mode is CAP Mode 2 TDS.\n");
                            break;
                        case MspCapData.MSP_CAP_MODE3:
                            logTextView.append("The signing request mode is CAP Mode 3.\n");
                            break;

                        default:
                            logTextView.append("The signing request mode is wrong!\n");

                    }
                } catch (MspException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onPause() {
        // Security Guideline: AND01. Sensitive data leaks
        ((TextView) this.findViewById(R.id.log))
                .setText("\tCleared for security reasons\n");

        super.onPause();

    }

}
