/*
 * ------------------------------------------------------------------------------
 * <p>
 * Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 * <p>
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 * <p>
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 * <p>
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.fake;

import com.gemalto.idp.mobile.core.IdpStorageException;
import com.gemalto.idp.mobile.otp.cap.CapService;
import com.gemalto.idp.mobile.otp.oath.OathService;
import com.gemalto.idp.mobile.otp.vic.VicService;

import java.util.Set;

/**
 * This class provides data that is expected to be retrieved from the user.
 * However, no UI is implemented as these methods are merely stubs that return
 * dummy values.
 */
public class FakeUi {

    /**
     * Fake UI for prompting the user for the account name (i.e. Token name).
     * Replace the auto-generated name if desired.
     */
    public static String promptForAccountName(Object otpService) {
        // The account name, which will be the token name, must be unique. Use
        // simple names but find one that is available first.
        Set<String> names = null;

        try {
            if (otpService instanceof OathService) {
                names = ((OathService) otpService).getTokenManager().getTokenNames();
            } else if (otpService instanceof CapService) {
                names = ((CapService) otpService).getTokenManager().getTokenNames();
            } else if (otpService instanceof VicService) {
                names = ((VicService) otpService).getTokenManager().getTokenNames();
            }
        } catch (IdpStorageException e) {
            e.printStackTrace();
        }

        char name = 'A';
        while (names.contains("" + name)) {
            name += 1;
        }
        return "" + name;
    }

    /**
     * Retrieve the existing tokenNames and select the token which is going to be used
     * Application can list the tokenNames for user to select, or
     * Application handles the logic and internally selects the token based on user action:
     * eg. login, transaction
     */
    public static String selectTokenName(Object otpService) {
        Set<String> names = null;
        String tokenName = null;
        try {
            if (otpService instanceof OathService) {
                names = ((OathService) otpService).getTokenManager().getTokenNames();
            } else if (otpService instanceof CapService) {
                names = ((CapService) otpService).getTokenManager().getTokenNames();
            } else if (otpService instanceof VicService) {
                names = ((VicService) otpService).getTokenManager().getTokenNames();
            }
            for (String name : names) {
                // Here the example application simply gets the first token
                // In real application, needs to select the token to be used
                tokenName = name;
                break;
            }
        } catch (IdpStorageException e) {
            e.printStackTrace();
        }
        return tokenName;
    }
}
