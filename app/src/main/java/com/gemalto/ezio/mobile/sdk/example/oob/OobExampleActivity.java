/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.oob.message.OobMessageAdapter;
import com.gemalto.ezio.mobile.sdk.example.oob.util.OobMessagingHelper;
import com.gemalto.ezio.mobile.sdk.example.oob.util.OobRegistrationHelper;
import com.gemalto.ezio.mobile.sdk.example.oob.view.GenericAlertDialog;
import com.gemalto.ezio.mobile.sdk.example.oob.view.GenericMessageDialog;
import com.gemalto.ezio.mobile.sdk.example.oob.view.ProgressBarHandler;
import com.gemalto.ezio.mobile.sdk.example.oob.view.RegistrationDialog;
import com.gemalto.ezio.mobile.sdk.example.oob.view.ReplyUserMsgDialog;
import com.gemalto.ezio.mobile.sdk.example.oob.view.UserMessageDialog;
import com.gemalto.ezio.mobile.sdk.example.oob.view.VerifyTransactionMessageDialog;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.oob.OobManager;
import com.gemalto.idp.mobile.oob.OobModule;
import com.gemalto.idp.mobile.oob.OobResponse;
import com.gemalto.idp.mobile.oob.message.OobGenericIncomingMessage;
import com.gemalto.idp.mobile.oob.message.OobIncomingMessage;
import com.gemalto.idp.mobile.oob.message.OobIncomingMessageType;
import com.gemalto.idp.mobile.oob.message.OobProviderToUserMessage;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyRequest;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyResponse.OobTransactionVerifyResponseValue;

/**
 * This class is the basic UI. It simply displays a TextView to log the results
 * and starts the app's logic.
 */
public class OobExampleActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String DATA_MESSAGE_ID = "DATA_MESSAGE_ID";

    public static final String PREF_KEY_CLIENT_ID = "clientId";

    private IdpCore core;
    private SharedPreferences pref;
    private OobRegistrationHelper oobRegistrationHelper;
    private OobMessagingHelper oobMessagingHelper;

    private TextView logTextView;

    private OobMessageAdapter adapter;

    private static Handler handler = new Handler(Looper.getMainLooper());

    private String messageId;

    private ProgressBarHandler progressBar;

    //################################################################################################
    // Activity life cycle methods
    //################################################################################################
    @Override
    @SuppressWarnings("ObsoleteSdkInt")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Security Guideline: AND01. Sensitive data leaks
        // Prevents screenshots of the app
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            Tools.disableScreenShot(this);
        }
        setContentView(R.layout.activity_oob_example);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialize UI components
        initUI();

        core = IdpCore.getInstance();

        // Initializing OOB module
        OobModule oobModule = OobModule.create();

        // Initialize OOB Manager
        OobManager oobManager = oobModule.createOobManager(FakeAppData.getOobUrl(),
                FakeAppData.getOobDomain(),
                FakeAppData.getOobApplicationId(),
                FakeAppData.getOobPublicKey());

        // persistent storage to manager clientId
        // developer can choose sqlite database
        pref = this.getSharedPreferences("oob.storage", Context.MODE_PRIVATE);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(DATA_MESSAGE_ID)) {
            messageId = extras.getString(DATA_MESSAGE_ID);
        }

        oobRegistrationHelper = OobRegistrationHelper.initialize(this, pref, oobManager);
        oobMessagingHelper = OobMessagingHelper.initialize(this, adapter, oobManager);
    }

    @Override
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(DATA_MESSAGE_ID)) {
            messageId = extras.getString(DATA_MESSAGE_ID);
            fetchMessageById(messageId);
        }
    }

    //################################################################################################
    // Core Oob functionality methods
    //################################################################################################

    /**
     * register the client
     *
     * @param userId           userId during enrolment
     * @param userAlias        userAlias
     * @param registrationCode registrationCode obtained from the provider
     */
    private void register(String userId, String userAlias, final SecureString
            registrationCode) {

        oobRegistrationHelper.register(userId, userAlias, registrationCode);
    }

    /**
     * Fetch message
     */
    private void fetchMessage() {

        // Retrieve the corresponding clientId
        String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);

        oobMessagingHelper.fetchMessage(clientId);
    }

    /**
     * Fetch message
     */
    private void fetchMessageById(String messageId) {

        // Retrieve the corresponding clientId
        String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);

        oobMessagingHelper.fetchMessageById(clientId, messageId);
    }

    /**
     * Acknowledge a message when needed
     *
     * @param message The incoming message
     */
    private void acknowledgeMessage(OobIncomingMessage message) {

        // Retrieve the corresponding clientId
        String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);

        oobMessagingHelper.acknowledgeMessage(clientId, message);
    }

    /**
     * Send reply to provider to user message
     *
     * @param message      The user message
     * @param replyTitle   The reply title
     * @param replyContent The reply content
     */
    private void sendReplyUserMessage(OobProviderToUserMessage message, String replyTitle, String replyContent) {

        // Retrieve the corresponding clientId
        String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);

        oobMessagingHelper.sendReplyUserMessage(clientId, message, replyTitle, replyContent);
    }

    /**
     * handle transaction verification request
     *
     * @param message The verify request message
     * @param value   Accept or Deny
     */
    private void handleTransactionVerification(OobTransactionVerifyRequest message, OobTransactionVerifyResponseValue value) {

        // Retrieve the corresponding clientId
        String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);

        oobMessagingHelper.handleTransactionVerification(clientId, message, value);
    }

    //################################################################################################
    // Utility methods
    //################################################################################################

    /**
     * Initialize the UI components
     */
    private void initUI() {

        Button registerButton = findViewById(R.id.registration);
        Button unregisterButton = findViewById(R.id.unregister_push);
        Button fetchButton = findViewById(R.id.fetch_message);
        logTextView = findViewById(R.id.log);
        ListView messageList = findViewById(R.id.message_list);
        progressBar = new ProgressBarHandler(this);

        adapter = new OobMessageAdapter(this);
        messageList.setAdapter(adapter);
        messageList.setOnItemClickListener(this);

        // Click registration button to start the registration process using
        // given registration code
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegistrationDialog();
            }
        });
        // Click un-registration button to un-register the OOB
        unregisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do {
                    String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);
                    if (TextUtils.isEmpty(clientId)) {
                        GenericAlertDialog dialog = new GenericAlertDialog(OobExampleActivity.this, "WARNING", "You have not registered to use OOB!");
                        dialog.show();

                        break;
                    }

                    oobRegistrationHelper.unregister(clientId);
                } while (false);
            }
        });
        // Click fetch button to fetch the message dispatched by the provider
        fetchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Check if registered
                String clientId = pref.getString(PREF_KEY_CLIENT_ID, null);
                if (clientId == null) {
                    GenericAlertDialog dialog = new GenericAlertDialog(OobExampleActivity.this, "WARNING", "You have not registered to use OOB!");
                    dialog.show();
                } else {
                    fetchMessage();
                }
            }
        });
    }

    /**
     * Display registration dialog
     */
    private void showRegistrationDialog() {

        final RegistrationDialog regDialog = new RegistrationDialog(this, new RegistrationDialog.RegisterDialogListener() {

            @Override
            public void onRegister(String registrationCode) {
                SecureString secureRegCode = core.getSecureContainerFactory().fromString(registrationCode);
                register(FakeAppData.getOobUserId(), FakeAppData.getOobUserAlias(), secureRegCode);
            }

            @Override
            public void onDismiss() { }
        });

        regDialog.show();
    }

    private void showUserMessageDialog(final OobProviderToUserMessage msg) {
        UserMessageDialog dialog = new UserMessageDialog(this, msg,
                new UserMessageDialog.UserMessageDialogListener() {
                    @Override
                    public void onReplyMessage(OobProviderToUserMessage message) {
                        showReplyUserMessageDialog(message);
                    }

                    @Override
                    public void onIgnoreMessage(OobProviderToUserMessage message) {
                        adapter.removeMessage(message);
                    }

                    @Override
                    public void onDismiss() { }
                });

        dialog.show();
    }

    private void showReplyUserMessageDialog(OobProviderToUserMessage msg) {
        ReplyUserMsgDialog dialog = new ReplyUserMsgDialog(this, msg,
                new ReplyUserMsgDialog.ReplyUserMsgDialogListener() {
                    @Override
                    public void onSendReply(OobProviderToUserMessage message, String
                            replyTitle, String replyContent) {
                        sendReplyUserMessage(message, replyTitle, replyContent);
                    }

                    @Override
                    public void onDismiss() { }
                });

        dialog.show();
    }

    private void showTransactionVerifyDialog(final OobTransactionVerifyRequest msg) {
        VerifyTransactionMessageDialog dialog = new VerifyTransactionMessageDialog(this, msg,
                new VerifyTransactionMessageDialog.TxMessageDialogListener() {
                    @Override
                    public void onAccept(OobTransactionVerifyRequest message) {
                        handleTransactionVerification(message, OobTransactionVerifyResponseValue.ACCEPTED);
                    }

                    @Override
                    public void onReject(OobTransactionVerifyRequest message) {
                        handleTransactionVerification(message, OobTransactionVerifyResponseValue.REJECTED);
                    }

                    @Override
                    public void onDismiss() { }
                });

        dialog.show();
    }

    private void showGenericMessageDialog(final OobGenericIncomingMessage msg) {
        GenericMessageDialog dialog = new GenericMessageDialog(this, msg, new GenericMessageDialog.GenericMessageDialogListener() {

            @Override
            public void onDismiss() {
                if (msg.isAcknowledgmentRequested()) {
                    acknowledgeMessage(msg);
                } else {
                    adapter.removeMessage(msg);
                }
            }
        });
        dialog.show();

    }

    /**
     * Append log
     */
    public void appendLog(final String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logTextView.append(message);
            }
        });
    }

    /**
     * Extract error message from response object
     *
     * @param response The response
     */
    public void handleErrorResponse(OobResponse response) {
        // Get Information from the response
        final int errorDomain = response.getDomain();
        final int errorCode = response.getCode();
        final String errorMessage = response.getMessage();

        // Get more detailed info if there is any
        final StringBuffer info = new StringBuffer();

        handler.post(new Runnable() {

            @Override
            public void run() {
                appendLog("\t Error Domain :" + errorDomain + "\n" +
                        "\t Error Code   :" + errorCode + "\n" +
                        "\t Error Message:" + errorMessage + "\n" +
                        info.toString());
            }
        });

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        OobIncomingMessage message = adapter.getItem(arg2);
        String messageType = message.getMessageType();

        if (messageType == null) return;

        switch (messageType) {
            case OobIncomingMessageType.GENERIC:
                showGenericMessageDialog((OobGenericIncomingMessage) message);
                break;

            case OobIncomingMessageType.USER_MESSAGE:
                showUserMessageDialog((OobProviderToUserMessage) message);
                break;

            case OobIncomingMessageType.TRANSACTION_VERIFY:
                showTransactionVerifyDialog((OobTransactionVerifyRequest) message);
                break;

            default:
                break;
        }
    }

    public void showProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.show();
            }
        });
    }

    public void hideProgressBar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.hide();
            }
        });
    }

}

