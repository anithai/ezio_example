/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp.task;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.otp.OtpSettings;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.authentication.mode.pin.PinAuthInput;
import com.gemalto.idp.mobile.core.ApplicationContextHolder;
import com.gemalto.idp.mobile.ui.UiModule;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputBuilderV2;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputService;
import com.gemalto.idp.mobile.ui.secureinput.SecureInputUi;
import com.gemalto.idp.mobile.ui.secureinput.SecurePinpadListenerV2;

import java.util.ArrayList;
import java.util.List;

import static com.gemalto.ezio.mobile.sdk.example.otp.OtpSettings.OBJECT_KEY;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SecurePinpadCustomFullScreenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SecurePinpadCustomFullScreenFragment extends Fragment implements SecurePinpadListenerV2 {

    private OtpSettings mSettings;

    onCustomFullScreenListener mListener;

    /**
     * SDK Limit for applying the secure flag depending upon sdk version
     */
    private static final int SECURE_FLAG_SDK_VERSION_LIMIT = 11;

    /**
     * Fragment Manager.
     */
    private FragmentManager mFragmentManager;
    /**
     * The SecureInputUi instance.
     */
    private SecureInputUi mSecureInputUi;
    /**
     * The dialogFragment.
     */
    private DialogFragment mDialogFragment;
    /**
     * SecureInputBuilder object
     */
    private SecureInputBuilderV2 pinpadBuilderV2;

    private FrameLayout mFrameLayout;

    /**
     * UI Config
     */
    private int pinLength = FakeAppData.getOtpPinLength();
    private TextView firstPinLabel;
    private TextView secondPinLabel;
    private TextView mainLabel;
    private TextView instruction;
    private EditText firstPinInput;
    private EditText secondPinInput;

    private Spannable modifiedText1;
    private Spannable modifiedText2;

    private ForegroundColorSpan span1;
    private ForegroundColorSpan span2;

    private StringBuilder firstInputBytes = new StringBuilder();
    private StringBuilder secondInputBytes = new StringBuilder();

    private int mCurrentSelectedInputField = 0;
    final Character inputFieldMaskCharacter = '●';

    private Integer darkBlueColor;
    private Integer lightBlueColor;
    private Integer redColor;
    private Integer greyColor;
    private Integer whiteColor;

    /*
     * Main keys
     */
    private static final String mMainKeys = "0123456789";

    /**
     * Subscripts
     */
    private List<String> mSubscripts = new ArrayList<String>() {
        {
            add("abc");
            add("d");
            add("ef");
            add(""); // Also allow empty string
            add("ghi");
            add("jklm");
            add("nopq");
            add("rst");
            add("uvw");
            add("xyz");
        }
    };

    public SecurePinpadCustomFullScreenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param otpSettings Parameter 1.
     * @return A new instance of fragment SecurePinpadCustomFullScreenFragment.
     */
    public static SecurePinpadCustomFullScreenFragment newInstance(OtpSettings otpSettings) {
        SecurePinpadCustomFullScreenFragment fragment = new SecurePinpadCustomFullScreenFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(OBJECT_KEY, otpSettings);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSettings = getArguments().getParcelable(OBJECT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_securepinpad_custon_full_screen, container, false);

        //UI preparation
        initColor();

        // Used to restrict user from taking screen shot.
        if (SECURE_FLAG_SDK_VERSION_LIMIT <= android.os.Build.VERSION.SDK_INT) {
            Tools.disableScreenShot(this.getActivity());
        }

        mainLabel = rootView.findViewById(R.id.Main_Label);
        firstPinLabel = rootView.findViewById(R.id.first_pin_label);
        secondPinLabel = rootView.findViewById(R.id.second_pin_label);
        instruction = rootView.findViewById(R.id.instruction);
        firstPinInput = rootView.findViewById(R.id.first_pin_input);
        secondPinInput = rootView.findViewById(R.id.second_pin_input);
        mFrameLayout = rootView.findViewById(R.id.frame_layout_v2_custom_full_screen);

        BuildSecureKeyPad();
        initCustomTopBarUIElement();
        configCustomizedTopBar();

        showSecureKeyPadAsFullScreen();

        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onCustomFullScreenListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface onCustomFullScreenListener {
        void onCustomFullScreenFinish(PinAuthInput firstPin, PinAuthInput secondPin);
    }

    /**
     * Initialize colors
     */
    private void initColor() {
        darkBlueColor = Color.parseColor("#1b346c");
        lightBlueColor = Color.parseColor("#00a4e6");
        redColor = Color.parseColor("#df083d");
        greyColor = Color.parseColor("#c3ced0");
        whiteColor = Color.parseColor("#ffffff");
    }

    private void initCustomTopBarUIElement() {

        firstPinLabel.setGravity(Gravity.RIGHT);
        secondPinLabel.setGravity(Gravity.RIGHT);
        mainLabel.setGravity(Gravity.LEFT);
        instruction.setGravity(Gravity.CENTER);


        firstPinInput.setInputType(InputType.TYPE_NULL);
        secondPinInput.setInputType(InputType.TYPE_NULL);

        firstPinInput.setGravity(Gravity.LEFT);
        secondPinInput.setGravity(Gravity.LEFT);

        firstPinInput.setBackgroundColor(Color.parseColor("#ffffff"));
        secondPinInput.setBackgroundColor(Color.parseColor("#ffffff"));

        mainLabel.setText("Ezio Secure Keypad Demo");

        // Fill input field with mask character, for example, dot or asterisk
        for (int i = 0; i != pinLength; i++) {
            firstInputBytes.append(inputFieldMaskCharacter);
            secondInputBytes.append(inputFieldMaskCharacter);
        }

        // Use spannable string to implement color masking
        modifiedText1 = new SpannableString(firstInputBytes.toString());
        modifiedText2 = new SpannableString(secondInputBytes.toString());

        // Initialize the first input field
        firstPinInput.setText(firstInputBytes.toString());
        secondPinInput.setText(secondInputBytes.toString());

        span1 = new ForegroundColorSpan(redColor);
        span2 = new ForegroundColorSpan(redColor);

        firstPinInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // Inform secure input UI when first custom input field gets focused
                    mSecureInputUi.selectInputField(0);
                    mCurrentSelectedInputField = 0;
                }
            }
        });

        secondPinInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // Inform secure input UI when second custom input field gets focused
                    mSecureInputUi.selectInputField(1);
                    mCurrentSelectedInputField = 1;
                }
            }
        });

    }

    private void configCustomizedTopBar() {
        if (!mSettings.isDoublePasswordMode()) {
            firstPinLabel.setText("Enter Pin");
            secondPinInput.setVisibility(View.GONE);
            secondPinLabel.setVisibility(View.GONE);

        } else {
            instruction.setText("Enter new PIN");
            firstPinLabel.setText("New Pin");
            secondPinLabel.setText("Confirm Pin");

            secondPinInput.setVisibility(View.VISIBLE);
            secondPinInput.setVisibility(View.VISIBLE);

            // Reset Pin 1 span
            modifiedText1.removeSpan(span1);
            firstPinInput.setText(modifiedText1);
        }
    }

    /* Try multiple config here */
    private void BuildSecureKeyPad() {

        // Get a SecureInputBuildV2 instance
        pinpadBuilderV2 = getSecureInputBuilder(this.getActivity().getApplicationContext());

        // Control button settings
        if (mSettings.isSwapControlButtons()) {
            pinpadBuilderV2.swapOkAndDeleteButton();
        }

        // Button press visibility settings
        pinpadBuilderV2.setButtonPressVisibility(mSettings.isVisualizeButtonPress());

        // Background
        pinpadBuilderV2.setScreenBackgroundColor(greyColor);

        // Button config
        pinpadBuilderV2.setButtonBorderWidth(2); // Looks not working....
        pinpadBuilderV2.setButtonBackgroundColor(SecureInputBuilderV2.UiControlState.NORMAL, whiteColor);
        pinpadBuilderV2.setButtonBackgroundColor(SecureInputBuilderV2.UiControlState.SELECTED, lightBlueColor);
        pinpadBuilderV2.setButtonBackgroundColor(SecureInputBuilderV2.UiControlState.DISABLED, greyColor);

        // Main keys must be set before the subscripts
        pinpadBuilderV2.setKeys(mMainKeys);
        pinpadBuilderV2.setKeyFontSize(30);
        pinpadBuilderV2.setKeyColor(SecureInputBuilderV2.UiControlState.NORMAL, darkBlueColor);
        pinpadBuilderV2.setKeyColor(SecureInputBuilderV2.UiControlState.SELECTED, darkBlueColor);
        pinpadBuilderV2.setKeyColor(SecureInputBuilderV2.UiControlState.DISABLED, darkBlueColor);

        // Subscripts settings
        pinpadBuilderV2.setSubscripts(mSubscripts);
        pinpadBuilderV2.setSubscriptFontSize(16);
        pinpadBuilderV2.setSubscriptColor(SecureInputBuilderV2.UiControlState.NORMAL, darkBlueColor);
        pinpadBuilderV2.setSubscriptColor(SecureInputBuilderV2.UiControlState.SELECTED, darkBlueColor);
        pinpadBuilderV2.setSubscriptColor(SecureInputBuilderV2.UiControlState.DISABLED, darkBlueColor);

        // Max and min length settings
        pinpadBuilderV2.setMaximumAndMinimumInputLength(pinLength, pinLength);

        // OK button settings
        pinpadBuilderV2.setOkButtonBehavior(SecureInputBuilderV2.OkButtonBehavior.CUSTOM);
        pinpadBuilderV2.setOkButtonText(getString(R.string.forget_pin));
        pinpadBuilderV2.setOkButtonFontSize(20);
        pinpadBuilderV2.setOkButtonTextColor(SecureInputBuilderV2.UiControlState.NORMAL, darkBlueColor);
        pinpadBuilderV2.setOkButtonTextColor(SecureInputBuilderV2.UiControlState.SELECTED, darkBlueColor);
        pinpadBuilderV2.setOkButtonTextColor(SecureInputBuilderV2.UiControlState.DISABLED, darkBlueColor);

        // Delete button settings
        pinpadBuilderV2.setDeleteButtonText(getString(R.string.delete_button_text));
        pinpadBuilderV2.setDeleteButtonFontSize(16);
        pinpadBuilderV2.setDeleteButtonTextColor(SecureInputBuilderV2.UiControlState.NORMAL, darkBlueColor);
        pinpadBuilderV2.setDeleteButtonTextColor(SecureInputBuilderV2.UiControlState.SELECTED, darkBlueColor);
        pinpadBuilderV2.setDeleteButtonTextColor(SecureInputBuilderV2.UiControlState.DISABLED, darkBlueColor);

        //4.6 new APIs
        pinpadBuilderV2.setKeypadFrameColor(Color.GREEN);
        pinpadBuilderV2.setKeypadGridGradientColors(Color.BLACK, Color.BLUE);
        pinpadBuilderV2.setIsDeleteButtonAlwaysEnabled(true);

        //4.8 new APIs
        pinpadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.LTGRAY, Color.WHITE);
        pinpadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinpadBuilderV2.setButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.LTGRAY);

        pinpadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.DKGRAY, Color.WHITE);
        pinpadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinpadBuilderV2.setOkButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.DKGRAY);

        pinpadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.NORMAL, Color.DKGRAY, Color.WHITE);
        pinpadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.SELECTED, Color.BLACK, Color.BLACK);
        pinpadBuilderV2.setDeleteButtonGradientColor(SecureInputBuilderV2.UiControlState.DISABLED, Color.WHITE, Color.DKGRAY);

        // The build-in top screen has to be hidden when user provides a custom top screen.
        pinpadBuilderV2.showTopScreen(false);

        mSecureInputUi = pinpadBuilderV2.buildPinpad(mSettings.isScrambleKeys(), mSettings.isDoublePasswordMode(), false, this);
    }

    /**
     * Show the keypad as view inside a framelayout.
     */
    private void showSecureKeyPadAsFullScreen() {

        mFragmentManager = getFragmentManager();
        mFrameLayout.setVisibility(View.VISIBLE);

        /* Frame Layout to hold the Keypad fragment. */
        mDialogFragment = mSecureInputUi.getDialogFragment();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(mFrameLayout.getId(), mDialogFragment);
        fragmentTransaction.commit();

        // Update keypad height and width.
        ViewTreeObserver viewTreeObserver = mFrameLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    if (android.os.Build.VERSION.SDK_INT < 16) {
                        Tools.removeLayoutListenerPre16(mFrameLayout.getViewTreeObserver(), this);
                    } else {
                        Tools.removeLayoutListenerPost16(mFrameLayout.getViewTreeObserver(), this);
                    }

                    int height = mFrameLayout.getHeight();
                    int width = mFrameLayout.getWidth();

                    pinpadBuilderV2.setKeypadHeight(height);
                    pinpadBuilderV2.setKeypadWidth(width);

                    mDialogFragment = mSecureInputUi.getDialogFragment();
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.detach(mDialogFragment);
                    fragmentTransaction.attach(mDialogFragment);
                    fragmentTransaction.commit();
                }
            });
        }
    }

    private SecureInputBuilderV2 getSecureInputBuilder(Context context) {
        // Create UiModule. It's the entry point for all UI relate features.
        UiModule uiModule = UiModule.create();

        // SecureInputBuildervV2 object
        SecureInputBuilderV2 pinpadBuilderV2;
        // Get a SecureInputBuilderV2 instance.
        pinpadBuilderV2 = SecureInputService.create(uiModule).getSecureInputBuilderV2();

        return pinpadBuilderV2;
    }

    /* Callbacks from the SecurePinpadListenerV2 */

    /**
     * Called when user finished entering the PIN on the keypad. In single password mode, the secondPin is null.
     * (the PIN text cannot be extracted from this secure data)
     */
    @Override
    public void onFinish(PinAuthInput firstPin, PinAuthInput secondPin) {

        // dismiss keypad, user can write your own dismiss method.
        // User has to dismiss it since the dialogFragment cannot be reused.
        // To user it, you have to create a new dialogFragment from pinpad object.
        dismissKeypad();

        /*
         * Now you can pass the secure data to the EzioSDK for changing the PIN or to generate OTP
         * It is recommended that you don't manipulate with the secure data object(s),
         * the SDK will clear it after use.
         */
        mListener.onCustomFullScreenFinish(firstPin, secondPin);
    }

    /**
     * Called when errors happened inside secure input.
     */
    @Override
    public void onError(String s) {
        Toast.makeText(getActivity(), "Error from Ezio SecureInput: " + s, Toast.LENGTH_SHORT).show();
    }

    /**
     * dismiss the keypad. The application maker can write their own method to
     * dismiss the keypad.
     */
    private void dismissKeypad() {
        // if it is displayed as a view
        if (mDialogFragment != null) {
            FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.remove(mDialogFragment);
            fragmentTransaction.commit();

            mFrameLayout.setVisibility(View.GONE);
        }

        mDialogFragment = null;

        // call the wipe method to clear the PIN from the keypad
        // and also the scrambling of the characters
        pinpadBuilderV2.wipe();
    }


    @Override
    public void onKeyPressedCountChanged(int newCount, int inputField) {
        if (inputField == 0) {
            if (newCount <= pinLength) {
                // Update UI for text 1
                modifiedText1.setSpan(span1, 0, newCount, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                firstPinInput.setText(modifiedText1);
            }
        } else {
            if (newCount <= pinLength) {

                modifiedText2.setSpan(span2, 0, newCount, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                secondPinInput.setText(modifiedText2);

            }
        }

        pinpadBuilderV2.setOkButtonText(getString(R.string.ok_button_text));
    }

    @Override
    public void onInputFieldSelected(int inputField) {
        if (inputField != mCurrentSelectedInputField) {
            if (inputField == 0) {
                firstPinInput.requestFocus();
            } else {
                secondPinInput.requestFocus();
            }
        }
    }

    @Override
    public void onOkButtonPressed() {
        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.ok_button_text) + " clicked.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteButtonPressed() {
        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.delete_button_text) + " clicked.", Toast.LENGTH_SHORT).show();
    }

}
