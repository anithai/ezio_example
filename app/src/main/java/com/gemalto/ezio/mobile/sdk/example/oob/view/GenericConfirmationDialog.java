/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPPEMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.R;

public class GenericConfirmationDialog extends Dialog implements android.view.View.OnClickListener {

    private Context context;
    private Button ok, cancel;
    private String title, body;
    private TextView dialogTitle, dialogBody;
    private ConfirmationDialogListener listener = null;

    public GenericConfirmationDialog(Context ctx, String title, String message, ConfirmationDialogListener confirmationDialogListener) {
        super(ctx);
        this.context = ctx;
        this.title = title;
        this.body = message;
        this.listener = confirmationDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_generic_confirmation);

        dialogTitle = findViewById(R.id.confirmation_dialog_title);
        dialogBody = findViewById(R.id.confirmation_dialog_body);

        dialogTitle.setText(this.title);
        dialogBody.setText(this.body);

        ok = findViewById(R.id.confirmation_btn_ok);
        cancel = findViewById(R.id.confirmation_btn_cancel);
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmation_btn_ok:
                listener.onConfirm();
                dismiss();
                break;
            case R.id.confirmation_btn_cancel:
                listener.onCancel();
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    public interface ConfirmationDialogListener {

        void onConfirm();

        void onCancel();

    }
}
