/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPPEMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyRequest;

public class VerifyTransactionMessageDialog extends Dialog implements android.view.View.OnClickListener {

    private Context context;
    private Button accpet, reject, cancel;
    private TextView messageSubject, messageContent;
    private TxMessageDialogListener listener = null;
    private OobTransactionVerifyRequest message = null;

    public VerifyTransactionMessageDialog(Context ctx, OobTransactionVerifyRequest msg, TxMessageDialogListener txMessageDialogListener) {
        super(ctx);
        this.context = ctx;
        this.message = msg;
        this.listener = txMessageDialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_verify_transaction);

        messageSubject = findViewById(R.id.tx_message_subject);
        messageContent = findViewById(R.id.tx_message_content);

        messageSubject.setText(message.getSubject().toString());
        messageContent.setText(message.getContentStr().toString());

        accpet = findViewById(R.id.btn_accept);
        reject = findViewById(R.id.btn_reject);
        cancel = findViewById(R.id.btn_cancel);
        accpet.setOnClickListener(this);
        reject.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_accept:
                listener.onAccept(message);
                break;
            case R.id.btn_reject:
                listener.onReject(message);
                break;
            case R.id.btn_cancel:
                listener.onDismiss();
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    public interface TxMessageDialogListener {

        void onAccept(OobTransactionVerifyRequest message);

        void onReject(OobTransactionVerifyRequest message);

        void onDismiss();

    }
}
