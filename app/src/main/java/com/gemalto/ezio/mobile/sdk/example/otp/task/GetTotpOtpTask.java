/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp.task;

import android.os.AsyncTask;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeUi;
import com.gemalto.ezio.mobile.sdk.example.otp.OtpSettings;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.authentication.AuthInput;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.IdpException;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManager;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.otp.OtpModule;
import com.gemalto.idp.mobile.otp.cap.CapDevice;
import com.gemalto.idp.mobile.otp.cap.CapService;
import com.gemalto.idp.mobile.otp.cap.CapToken;
import com.gemalto.idp.mobile.otp.oath.OathDevice;
import com.gemalto.idp.mobile.otp.oath.OathService;
import com.gemalto.idp.mobile.otp.oath.OathToken;
import com.gemalto.idp.mobile.otp.vic.VicService;

import java.lang.ref.WeakReference;

public class GetTotpOtpTask extends AsyncTask<Void, CharSequence, Void> {

    // The text view to update with the results
    private WeakReference<TextView> viewToUpdate;
    private String currentTaskName;
    private AuthInput authInput;
    private OathService oathService;
    private String tokenName;

    public GetTotpOtpTask(AuthInput authInput, TextView viewToUpdate) {
        this.authInput = authInput;

        // Create OtpModule. It's the entry point for all Otp relate features.
        OtpModule otpModule = OtpModule.create();
        // Create OathService. It's the entry point for all oath relate features.
        this.oathService = OathService.create(otpModule);

        this.tokenName = FakeUi.selectTokenName(this.oathService);

        this.viewToUpdate = new WeakReference<>(viewToUpdate);
    }

    /**
     * This method starts the example logic but does not serve as any kind of
     * example for the Ezio Mobile SDK.
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            // LIST TOKENS AND GENERATE OTPs
            // Note: For a real application, listing tokens and generating OTPs
            // is fast enough to be done in the UI thread. The application may
            // decide if this is done in a separate execution thread.
            currentTaskName = "Generate OTP";

            if (authInput == null) publishProgress("Pin is null!");
            generateOtps();
        } catch (Exception e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(CharSequence... textToAppend) {
        TextView txtView = viewToUpdate.get();
        if (txtView != null) {
            txtView.append(Tools.generateLogTitle(currentTaskName));

            for (CharSequence cs : textToAppend) {
                txtView.append("\t" + cs + "\n");
            }
        }
    }

    /**
     * This method will generate a OATH TOTP for all the tokens provisioned
     * in the device. It provides a real world example (minus the bad UI) of
     * how this should be performed and it includes all security guidelines that
     * must be followed.
     */
    private void generateOtps() {
        SecureString otp = null;

        try {
            // The same custom fingerprint data used to create the token must
            // be provided before retrieving the token.
            byte[] fingerprintData = FakeAppData.getCustomFingerprintData();

            // GENERATE OATH TOTP

            // Notice that the fingerprint is included when creating the token.
            OathToken oathToken = oathService.getTokenManager().getToken(tokenName, fingerprintData);

            // Create OathDevice that generates OTPs using default settings.
            // Notice this will create OATH TOTPs.
            OathDevice oathDevice = oathService.getFactory().createSoftOathDevice(oathToken);

            // To change the OATH Device Settings (i.e OcraOtpLength, HashAlgorithm & etc),
            // create & specify the settings with the following:
            //
            // SoftOathSettings settings = oathService.getFactory().createSoftOathSettings();
            //
            // For more information on changing the OATH Device Settings, please refer to
            // chapter 'OATH Device Settings' of the Programmer's Guide.

            otp = oathDevice.getTotp(authInput);

            // Security Guideline: AND01. Sensitive data leaks
            // The OTP will remain displayed in the UI even after this
            // local
            // variable is wiped. The application should clear this
            // information as soon as it is consumed by the user (in
            // this example's case, until the app is paused).
            publishProgress("Current token used:" + tokenName + "\n\t" +
                    "OTP Type: TOTP " + " OTP = " + otp + "\n\t" +
                    "Last token is taken from the database. " +
                    "Please modify the code if you wish to select among different tokens.\n\t");
        } catch (IdpException e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
            e.printStackTrace();
        } finally {
            // Security Guideline: GEN12. Wiping assets
            // The OTP must be wiped after use.
            if (otp != null) {
                otp.wipe();
            }

            // Security Guideline: GEN08. PIN sanitization
            // The PIN must be wiped ASAP
            if (authInput != null) {
                authInput.wipe();
            }
        }
    }
}
