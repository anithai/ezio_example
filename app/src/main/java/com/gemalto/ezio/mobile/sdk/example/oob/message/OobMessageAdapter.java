/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPPEMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.oob.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.R;
import com.gemalto.idp.mobile.oob.message.OobGenericIncomingMessage;
import com.gemalto.idp.mobile.oob.message.OobIncomingMessage;
import com.gemalto.idp.mobile.oob.message.OobIncomingMessageType;
import com.gemalto.idp.mobile.oob.message.OobProviderToUserMessage;
import com.gemalto.idp.mobile.oob.message.OobTransactionVerifyRequest;

import java.util.ArrayList;
import java.util.List;

public class OobMessageAdapter extends BaseAdapter {

    private Context context;
    private List<OobIncomingMessage> data;

    public OobMessageAdapter(Context ctx) {
        this.context = ctx;
        data = new ArrayList<OobIncomingMessage>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OobIncomingMessage getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.row_message, parent, false);
        }

        OobIncomingMessage item = data.get(position);
        decorateRowMessage(item, convertView);

        return convertView;
    }

    public void addMessage(OobIncomingMessage message) {
        if (!contains(message)) {
            data.add(message);
            notifyDataSetChanged();
        }
    }

    public void removeMessage(OobIncomingMessage message) {
        if (contains(message)) {
            data.remove(getPositionOfMessage(message));
            notifyDataSetChanged();
        }
    }

    public List<OobIncomingMessage> getAllMessages() {
        return data;
    }

    public OobIncomingMessage getMessage(int position) {
        return data.get(position);
    }

    private boolean contains(OobIncomingMessage message) {
        for (OobIncomingMessage msg : data) {
            if (msg.getMessageId().equals(message.getMessageId())) {
                return true;
            }
        }
        return false;
    }

    private int getPositionOfMessage(OobIncomingMessage message) {
        for (int i = 0; i < data.size(); i++) {
            OobIncomingMessage msg = data.get(i);
            if (msg.getMessageId().equals(message.getMessageId())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Extract and display the message content
     *
     * @param message
     */
    private void decorateRowMessage(OobIncomingMessage message, View view) {

        TextView contentView = (TextView) view.findViewById(R.id.message_content);
        TextView expirationDateView = (TextView) view.findViewById(R.id.message_expiration_date);

        String messageType = message.getMessageType();

        if (messageType.equals(OobIncomingMessageType.GENERIC)) {
            contentView.setText("Content : " + ((OobGenericIncomingMessage) message).getContentStr());
        } else if (messageType.equals(OobIncomingMessageType.USER_MESSAGE)) {
            contentView.setText("Content : " + ((OobProviderToUserMessage) message).getContentStr());
        } else if (messageType.equals(OobIncomingMessageType.TRANSACTION_VERIFY)) {
            contentView.setText("Content : " + ((OobTransactionVerifyRequest) message).getContentStr());
        }
        expirationDateView.setText("EXPIRES ON : " + message.getExpirationDate());
    }
}
