/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.msp.MspExampleActivity;
import com.gemalto.ezio.mobile.sdk.example.oob.OobExampleActivity;
import com.gemalto.ezio.mobile.sdk.example.otp.OtpExampleActivity;
import com.gemalto.ezio.mobile.sdk.example.util.Hex;
import com.gemalto.idp.mobile.core.ApplicationContextHolder;
import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.devicefingerprint.DeviceFingerprintSource;
import com.gemalto.idp.mobile.core.net.TlsConfiguration;
import com.gemalto.idp.mobile.msp.MspConfiguration;
import com.gemalto.idp.mobile.msp.MspSignatureKey;
import com.gemalto.idp.mobile.oob.OobConfiguration;
import com.gemalto.idp.mobile.otp.OtpConfiguration;

import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    // This activation code would enable the Oob and SecurePinPad feature,
    // Put the correct value of activationCode and pass it when initializing IdpCore
    private final byte[] EZIO_ACTIVATION_CODE = Hex.compress("01414752424e4b3031000000060ba9c876509c01701bbce67ec2711cb379cac79b2032cb0cf456b97cd824378b8f28187c51d848340868eb58f47e3eed416259aa1685b994d11f0556e82f0292");

    public static IdpCore core;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // region Configure IdpCore

        // The entry point for all Ezio Mobile SDK.
        // The Android Context object enables the SDK to create databases and
        // files on behalf of the application.
        ApplicationContextHolder.setContext(this.getApplicationContext());
        if (!IdpCore.isConfigured()) {
            core = IdpCore.configure(EZIO_ACTIVATION_CODE, getOtpConfiguration(), getOobConfiguration(), getMspConfiguration());
        } else {
            core = IdpCore.getInstance();
        }

        // NOTE: the PasswordManager login/logout has been implemented in the {@code com.gemalto.ezio.mobile.sdk.example.MyApplication}.
        // Check there for more information.

        // endregion

        Button buttonOtpExample = findViewById(R.id.buttonOTP);
        buttonOtpExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, OtpExampleActivity.class);
                startActivity(intent);
            }
        });

        Button buttonOobExample = findViewById(R.id.buttonOOB);
        buttonOobExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, OobExampleActivity.class);
                startActivity(intent);
            }
        });

        Button buttonMspExample = findViewById(R.id.buttonMSP);
        buttonMspExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, MspExampleActivity.class);
                startActivity(intent);
            }
        });
    }

    // region Module configuration
    private OtpConfiguration getOtpConfiguration() {
        // Initialize otp configuration
        // TokenRootPolicy determine the SDK behaviour in the event of Root is detected on the device
        return new OtpConfiguration.Builder()
                .setRootPolicy(OtpConfiguration.TokenRootPolicy.REMOVE_ALL_TOKENS)
                .build();
    }

    /**
     * Intialize Oob configurations
     */
    private OobConfiguration getOobConfiguration() {
        // 1. TLS configurations
        // Security Guideline: GEN02. Communication over HTTP forbidden
        // Security Guideline: GEN04. Reject invalid SSL certificates
        //
        // Use this to lower the security of the TLS connection. It is preferred
        // to not alter the TLS settings in order to ensure the highest level of
        // security.
        TlsConfiguration tlsConfig;
        if (BuildConfig.DEBUG) {
            tlsConfig = new TlsConfiguration(
                    TlsConfiguration.Permit.HOSTNAME_MISMATCH,
                    TlsConfiguration.Permit.SELF_SIGNED_CERTIFICATES);
        } else {
            tlsConfig = new TlsConfiguration();
        }

        // 2. Device fingerprint source
        DeviceFingerprintSource dfs = new DeviceFingerprintSource(FakeAppData.getCustomFingerprintData());

        // 3. build OobConfiguration
        OobConfiguration oobConfig = new OobConfiguration.Builder()
                // Optional Step:
                // This is unnecessary if one uses the default TlsConfiguration.
                // However, this example leaves this call in order to demonstrate how
                // to use the TLS options.
                .setTlsConfiguration(tlsConfig)
                // Optional Step:
                // Set root detection policy.
                // If it's not set, the default root detection policy OobRootPolicy.FAIL
                // will be used.
                .setRootPolicy(OobConfiguration.OobRootPolicy.FAIL)
                // Optional Step:
                // Set device fingerprint source.
                // If it's not set, the default device fingerprint source settings:
                // Type.SERVICE and Type.SOFT
                // will be used.
                .setDeviceFingerprintSource(dfs)
                .build();

        return oobConfig;
    }

    private MspSignatureKey signKey = new MspSignatureKey(
            FakeAppData.getMspSignaturePublicKey(),
            FakeAppData.getMspSignatureP(),
            FakeAppData.getMspSignatureQ(),
            FakeAppData.getMspSignatureG()
    );

    private MspConfiguration getMspConfiguration() {
        return new MspConfiguration.Builder()
                .setSignatureKeys(Collections.singletonList(signKey))
                .setObfuscationKeys(Collections.singletonList(FakeAppData.getMspObfuscationKey()))
                .build();
    }
    //endregion

}
