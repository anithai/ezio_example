/*
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */

package com.gemalto.ezio.mobile.sdk.example.otp;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class OtpSettings implements Parcelable {

    /**
     * Targeted OTP Type
     */
    public enum ENU_OTP_TYPE {
        TOTP,
        CAP,
        VIC,
    }

    /**
     * SecurePinPad displaying mode
     */
    public enum ENU_DISPLAY_MODE {
        Dialog,
        Fullscreen,
        CustomFullscreen,
    }

    public static String OBJECT_KEY = "otp.custom.settings";

    private ENU_OTP_TYPE otpType;
    private boolean enableSecurePinPad;

    private boolean isScrambleKeys;
    private boolean isSwapControlButtons;
    private boolean isVisualizeButtonPress;
    private boolean isSimulateOkButtonPress;
    private boolean isTouchRippleSetting;
    private boolean isDoublePasswordMode;

    private ENU_DISPLAY_MODE mDisplayMode;

    public OtpSettings() {
        otpType = ENU_OTP_TYPE.TOTP;
        enableSecurePinPad = false;

        isScrambleKeys = true;
        isSwapControlButtons = false;
        isVisualizeButtonPress = true;
        isSimulateOkButtonPress = false;
        isTouchRippleSetting = false;

        mDisplayMode = ENU_DISPLAY_MODE.Dialog;
    }

    private OtpSettings(Parcel in) {
        String value = in.readString();
        if (!TextUtils.isEmpty(value))
            otpType = ENU_OTP_TYPE.valueOf(value);

        isScrambleKeys = in.readByte() != 0;
        isSwapControlButtons = in.readByte() != 0;
        isVisualizeButtonPress = in.readByte() != 0;
        isSimulateOkButtonPress = in.readByte() != 0;
        isTouchRippleSetting = in.readByte() != 0;

        value = in.readString();
        if (!TextUtils.isEmpty(value))
            mDisplayMode = ENU_DISPLAY_MODE.valueOf(value);
    }

    public static final Creator<OtpSettings> CREATOR = new Creator<OtpSettings>() {
        @Override
        public OtpSettings createFromParcel(Parcel in) {
            return new OtpSettings(in);
        }

        @Override
        public OtpSettings[] newArray(int size) {
            return new OtpSettings[size];
        }
    };

    public ENU_OTP_TYPE getOtpType() {
        return otpType;
    }

    public void setOtpType(ENU_OTP_TYPE mOTPType) {
        this.otpType = mOTPType;
    }

    public boolean isScrambleKeys() {
        return isScrambleKeys;
    }

    public void setScrambleKeys(boolean isScramble) {
        this.isScrambleKeys = isScramble;
    }

    public boolean isSwapControlButtons() {
        return isSwapControlButtons;
    }

    public void setSwapControlButtons(boolean isSwapControl) {
        this.isSwapControlButtons = isSwapControl;
    }

    public boolean isVisualizeButtonPress() {
        return isVisualizeButtonPress;
    }

    public void setVisualizeButtonPress(boolean isVisualize) {
        this.isVisualizeButtonPress = isVisualize;
    }

    public boolean isSimulateOkButtonPress() {
        return isSimulateOkButtonPress;
    }

    public void setSimulateOkButtonPress(boolean isSimulate) {
        this.isSimulateOkButtonPress = isSimulate;
    }

    public boolean isTouchRippleSetting() {
        return isTouchRippleSetting;
    }

    public void setTouchRippleSetting(boolean isTouchRipple) {
        this.isTouchRippleSetting = isTouchRipple;
    }

    public boolean isDoublePasswordMode() {
        return isDoublePasswordMode;
    }

    public void setDoublePasswordMode(boolean isDoublePassword) {
        this.isDoublePasswordMode = isDoublePassword;
    }

    public void setDisplayMode(ENU_DISPLAY_MODE mode) {
        mDisplayMode = mode;
    }

    public ENU_DISPLAY_MODE getDisplayMode() {
        return mDisplayMode;
    }

    public void setEnableSecurePinPad(boolean enabled) {
        enableSecurePinPad = enabled;
    }

    public boolean isEnableSecurePinPad() {
        return enableSecurePinPad;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(otpType == null ? null : otpType.name());
        dest.writeByte((byte) (isScrambleKeys ? 1 : 0));
        dest.writeByte((byte) (isSwapControlButtons ? 1 : 0));
        dest.writeByte((byte) (isVisualizeButtonPress ? 1 : 0));
        dest.writeByte((byte) (isSimulateOkButtonPress ? 1 : 0));
        dest.writeByte((byte) (isTouchRippleSetting ? 1 : 0));
        dest.writeString(mDisplayMode == null ? null : mDisplayMode.name());
    }
}
