/**
 * ------------------------------------------------------------------------------
 *
 *     Copyright (c) 2019  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */
package com.gemalto.ezio.mobile.sdk.example.otp.task;

import android.os.AsyncTask;
import android.widget.TextView;

import com.gemalto.ezio.mobile.sdk.example.fake.FakeAppData;
import com.gemalto.ezio.mobile.sdk.example.fake.FakeUi;
import com.gemalto.ezio.mobile.sdk.example.otp.OtpSettings;
import com.gemalto.ezio.mobile.sdk.example.util.AsyncResponse;
import com.gemalto.ezio.mobile.sdk.example.util.Tools;
import com.gemalto.idp.mobile.core.devicefingerprint.DeviceFingerprintSource;
import com.gemalto.idp.mobile.core.net.TlsConfiguration;
import com.gemalto.idp.mobile.core.util.SecureString;
import com.gemalto.idp.mobile.otp.OtpModule;
import com.gemalto.idp.mobile.otp.devicefingerprint.DeviceFingerprintTokenPolicy;
import com.gemalto.idp.mobile.otp.oath.OathService;
import com.gemalto.idp.mobile.otp.provisioning.EpsConfigurationBuilder;
import com.gemalto.idp.mobile.otp.provisioning.MobileProvisioningProtocol;
import com.gemalto.idp.mobile.otp.provisioning.ProvisioningConfiguration;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

public class ProvisioningTask extends AsyncTask<Void, CharSequence, Void> {

    // The entry point for the Ezio Mobile SDK
    private OtpSettings.ENU_OTP_TYPE otpType;
    private OathService oathService;
//    private CapService capService;
//    private VicService vicService;

    // The text view to update with the results
    private WeakReference<TextView> viewToUpdate;
    private SecureString registrationCode;

    private static final String TASK_NAME = "PROVISIONING";

    // Response to main class
    private AsyncResponse delegate;

    public ProvisioningTask(OtpSettings.ENU_OTP_TYPE otpType,
                            SecureString registrationCode,
                            TextView txtLogView, AsyncResponse delegate
    ) {
        this.otpType = otpType;

        // Create OtpModule. It's the entry point for all Otp relate features.
        OtpModule otpModule = OtpModule.create();
        // Create OathService. It's the entry point for all oath relate features.
        this.oathService = OathService.create(otpModule);
        // Create CapService. It's the entry point for all Cap relate features.
//        this.capService = CapService.create(otpModule);
        // Create VicService. It's the entry point for all Vic relate features.
//        this.vicService = VicService.create(otpModule);

        this.registrationCode = registrationCode;
        this.viewToUpdate = new WeakReference<>(txtLogView);
        this.delegate = delegate;
    }

    /**
     * This method starts the example logic but does not serve as any kind of
     * example for the Ezio Mobile SDK.
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            // PROVISIONING
            // Note: For a real application, the provisioning will take a long time to execute
            // and therefore some form of UI should be displayed.
            provision();

        } catch (Exception e) {
            publishProgress("Exception: " + e.getClass().getSimpleName() + "\n\tMessage: " + e.getMessage() + "\n");
        }

        return null;
    }

    /* UI update */
    protected void onProgressUpdate(CharSequence... textToAppend) {
        TextView view = viewToUpdate.get();
        if (view != null) {
            view.append(Tools.generateLogTitle(TASK_NAME));

            for (CharSequence cs : textToAppend) {
                view.append("\t" + cs + "\n");
            }
        }
    }

    /**
     * This method will provision the device with a new user (if successful). It
     * provides a real world example (minus the fake UI) of how this should be
     * performed and it includes all security guidelines that must be followed.
     */
    private void provision() {
        // Security Guideline: GEN20. Atomicity of personalization
        // Data to wipe if there is a failure
        boolean failure = true;

        try {
            // CONFIGURE THE TOKEN BUILDER: See each setup step for further details.
            ProvisioningConfiguration epsConfig = setupEpsConfiguration();

            // SETUP FINGERPRINT TOKEN POLICY: See more details in the setup method
            DeviceFingerprintTokenPolicy fingerprint = setupFingerprintTokenPolicy();

            // FAKE UI: The application needs to ask the user for this
            // information before creating the token. You can freely alter the
            // FakeUi class to alter the token name, etc.
            String tokenName = null;
            switch (otpType) {
                case TOTP: {
                    tokenName = FakeUi.promptForAccountName(oathService);
                    oathService.getTokenManager().createToken(tokenName, epsConfig, fingerprint);
                    publishProgress("OTP Type: " + "TOTP");
                    break;
                }

//                case CAP: {
//                    tokenName = FakeUi.promptForAccountName(capService);
//                    capService.getTokenManager().createToken(tokenName, epsConfig, fingerprint);
//                    publishProgress("OTP Type: " + "CAP");
//                    break;
//                }
//
//                case VIC: {
//                    tokenName = FakeUi.promptForAccountName(vicService);
//                    vicService.getTokenManager().createToken(tokenName, epsConfig, fingerprint);
//                    publishProgress("OTP Type: " + "VIC");
//                    break;
//                }

                default:
                    break;
            }

            // Security Guideline: AND07. Clearing assets before a TLS session (e.g. provisioning)
            // The registration code must be wiped after token builder creation.
            registrationCode.wipe();

            // CREATE THE TOKEN: This will establish a data connection with the
            // URL provided to the EpsConfiguration (see setupEpsConfiguration)
            // in order to retrieve the token credentials.
            publishProgress("Creating Token: " + tokenName);

            failure = false;
            publishProgress("Token created: " + tokenName);

            // Pass token name back to main class
            delegate.processFinish(tokenName);
        } catch (Exception e) {
            publishProgress("Exception: " + e.getClass().getSimpleName(),
                    "Exception message: " + e.getMessage(),
                    "---",
                    "* Verify the URL is correct",
                    "* Try generating a new PIN and registration code in the EPS",
                    "* Is the URL insecure?  Then configure the TlsConfiguration to permit insecure connections");
            e.printStackTrace();
        } finally {
            if (failure) {
                // Security Guideline: GEN12. Wiping assets
                // The registration code must be wiped after use.
                if (registrationCode != null) {
                    registrationCode.wipe();
                }
            }
        }
    }

    /**
     * Setup the EPS configuration values. The inputs come from the FakeAppData
     * class.
     */
    private ProvisioningConfiguration setupEpsConfiguration() throws MalformedURLException {
        URL epsUrl = new URL(FakeAppData.getOtpEpsUrl(this.otpType));

        ProvisioningConfiguration configuration;

        // Security Guideline: GEN02. Communication over HTTP forbidden
        // Security Guideline: GEN04. Reject invalid SSL certificates
        //
        // Use this to lower the security of the TLS connection. It is preferred
        // to not alter the TLS settings in order to ensure the highest level of
        // security. You can only reduce the security using debug version of
        // the SDK
        TlsConfiguration tls = new TlsConfiguration(1000,
                TlsConfiguration.Permit.INSECURE_CONNECTIONS);


        // Security Guideline: Certificate pinning
        // Key pin store has been configure to trust only certificate from Gemalto EPS
        // An exception will be issued if try to connect to other address
        MobileProvisioningProtocol provisioningProtocol;
        if (this.otpType == OtpSettings.ENU_OTP_TYPE.VIC) {
            provisioningProtocol = MobileProvisioningProtocol.PROVISIONING_PROTOCOL_V1;
        } else {
            provisioningProtocol = MobileProvisioningProtocol.PROVISIONING_PROTOCOL_V3;
        }
        configuration = new EpsConfigurationBuilder(
                registrationCode, epsUrl,
                provisioningProtocol,
                FakeAppData.getOtpRsaKeyId(this.otpType),
                FakeAppData.getOtpEpsRsaKeyExponent(),
                FakeAppData.getOtpEpsRsaKeyModulus(this.otpType))
                // Optional Step:
                // This is unnecessary if one uses the default TlsConfiguration.
                // However, this example leaves this call in order to demonstrate how
                // to use the TLS options.
                .setTlsConfiguration(tls)
                .build();

        return configuration;
    }

    /**
     * Setup the fingerprint source. This data will seal the user credentials to
     * this device, SIM card, etc., so an attacker cannot simply copy the
     * device's raw data.
     */
    private DeviceFingerprintTokenPolicy setupFingerprintTokenPolicy() {
        byte[] customFingerprint = FakeAppData.getCustomFingerprintData();

        // Security Guideline: AND02. Anti-cloning data
        // Use all available fingerprinting features
        return new DeviceFingerprintTokenPolicy(true,
                new DeviceFingerprintSource(customFingerprint, DeviceFingerprintSource.Type.SOFT));
    }
}
